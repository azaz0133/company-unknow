import * as Hapi from "hapi";
import { createConnection } from "typeorm";
import { authCheck } from "./middleware/auth";
import { swaggerOptions } from "./utils/swagger";
import * as moment from "moment";
import { connectionToDatabase } from "./utils/connectDB";

require("dotenv").config();

const year = moment().format("YYYY");
const month = moment().format("MMM");
const day = moment().format("Do");

export const initServer: Function = async routes => {
  const Server: any = new Hapi.Server({
    host: process.env.HOST || "127.0.0.1",
    port: process.env.PORT || 8081,
    compression: false,
    routes: {
      cors: {
        additionalHeaders: [
          "Access-Control-Allow-Origin",
          "Access-Control-Request-Method",
          "Access-Control-Request-Headers",
          "Allow-Origin",
          "Origin",
          "access-control-allow-origin",
          "access-control-request-method",
          "allow-origin",
          "origin",
          "Authorization"
        ]
      }
    }
  });

  process.env.NODE_ENV == "PRODUCTION"
    ? await connectionToDatabase()
    : await createConnection();

  /* Plugins initialize */
  await Server.register(require("hapi-auth-jwt2"));

  await Server.register(require("vision"));

  // await Server.register({
  //   plugin: require("good"),
  //   options: {
  //     ops: {
  //       interval: 1000
  //     },
  //     reporters: {
  //       myConsoleReporter: [
  //         {
  //           module: "good-squeeze",
  //           name: "Squeeze",
  //           args: [{ log: "*", response: "*" }]
  //         }
  //       ],
  //       file: [
  //         {
  //           module: "good-squeeze",
  //           name: "Squeeze",
  //           args: [{ ops: "*" }]
  //         },
  //         {
  //           module: "good-squeeze",
  //           name: "SafeJson"
  //         },
  //         {
  //           module: "good-file",
  //           args: [`${__dirname}/../logs/${year}/${month}/${day}`]
  //         }
  //       ]
  //     }
  //   }
  // });

  // await Server.register({
  //   plugin: require("crumb")
  // });

  Server.auth.strategy("jwt", "jwt", {
    key: process.env.SECRET_KEY || "sellgas",
    validate: authCheck,
    verifyOptions: { algorithms: ["HS256"] }
  });
  await Server.register({
    plugin: require("hapi-authorization")
  });

  /* TEST API DOCUMENT AVAILABLE ONLY DEVELOPMENT   */
  await Server.register(require("inert"));
  // if (process.env.NODE_ENV.includes("DEVELOPMENT")) {
  await Server.register([
    require("vision"),
    {
      plugin: require("hapi-swagger"),
      options: swaggerOptions
    }
  ]);
  // }
  /* ending regiter Plugins */
  Server.views({
    engines: {
      html: {
        module: require("handlebars"),
        compileMode: "sync" // engine specific
      }
    },
    layout: "printpdf",
    compileMode: "async", // global setting
    path: __dirname + "/views"
  });

  await setupRoutes(Server, routes);

  await startServer(Server);
};

const startServer: Function = async (Server: Hapi.Server) => {
  try {
    await Server.start();
  } catch (err) {
    console.log(err);
    process.exit(1);
  }

  console.log(`Server running at  ${Server.info.uri}`);
};

const setupRoutes = (Server: Hapi.Server, routes) => {
  /* register all routes */
  console.log("Registering routes");
  Server.route({
    method: "GET",
    path: "/testpdf.pdf",
    options: {},
    handler: (req, h: any) => {
      return h.file("./src/assets/invoices/test.pdf");
    }
  });
  Server.route({
    method: "GET",
    path: "/printpdf",
    handler: (req, h: any) => {
      return h.file("./src/pdf.html");
    }
  });
  Server.route(routes);
};
