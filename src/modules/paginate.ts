import { Finder } from "./finder";

export class Pagination extends Finder {
  constructor(key: string, relations: Array<string>) {
      super(key, relations);
  }

  paginate(
    conditions = {},
    page: number = 1,
    perPage: number = 5
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      const query = this.where(conditions).then(async (results: any) => {
        resolve({
          [this.keyNameOfEntity]: results[this.keyNameOfEntity].slice(
            (page - 1) * perPage * perPage
          ),
          meta: {
            page,
            perPage,
            totalPages: Math.ceil(results.length / perPage)
          }
        });
      });
    });
  }
}
