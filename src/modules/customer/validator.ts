import * as Joi from "joi";

export const customerValidate = {
  create: {
    payload: Joi.object({
      firstName: Joi.string()
        .required()
        .example("อนิรัด"),
      lastName: Joi.string()
        .required()
        .example("หวะดี"),
      address: Joi.string()
        .required()
        .example(
          "2225/35 sadda asdsad;l sadlkkklsa ksdalsaposa msamsa am,sa;kl"
        ),
      phoneNumber: Joi.string()
        .required()
        .example("0813236656"),
      tin: Joi.string()
        .required()
        .example("1100212022"),
      detail: Joi.string()
        .required()
        .example("aslksakdha afs askdsasad jsadh ksadksasad ads;kh"),
      organization: Joi.string().required(),
      email: Joi.string()
        .email()
        .required()
        .example("test@mail.com"),
      customerType: Joi.string()
        .required()
        .example("nuomal")
    }),
    headers: Joi.object({
      authorization: Joi.any().required()
    })
  },
  getOne: {
    params: Joi.object({
      id: Joi.string()
        .required()
        .example("C0001")
    })
  },
  editByid: {
    params: Joi.object({
      id: Joi.string()
        .required()
        .example("C0001")
    }),
    payload: Joi.object({
      firstName: Joi.string()
        .required()
        .example("worawit"),
      lastName: Joi.string()
        .required()
        .example("puto"),
      address: Joi.string()
        .required()
        .example("road 2252 as asd sad "),
      phoneNumber: Joi.string().required(),
      tin: Joi.string()
        .required()
        .example("110050146325"),
      detail: Joi.string()
        .required()
        .example("askjsadj nasdlsad nkdsa klsa hksd"),
      prefixName: Joi.string()
        .required()
        .example("Mr."),
      organization: Joi.string()
        .required()
        .example("wongnai"),
      postcode: Joi.string()
        .required()
        .example("12021"),
      branch: Joi.string()
        .required()
        .example("managet"),
      faxNumber: Joi.string()
        .required()
        .example("2022-53-565"),
      contact: Joi.string()
        .required()
        .example("no one"),
      latitude: Joi.number()
        .required()
        .example(11550000001.2000000522),
      longtitude: Joi.number()
        .required()
        .example(1224.6422),
      cashCredit: Joi.string()
        .required()
        .example("credit card"),
      email: Joi.string()
        .required()
        .email()
        .example("test@gmail.com"),
      amountCredit: Joi.number()
        .required()
        .min(0)
        .max(99999999)
        .example(10000),
      paymentConditions: Joi.string()
        .required()
        .example("cash"),
      gasCalculation: Joi.string()
        .required()
        .example("s saSA"),
      priceDetermination: Joi.string().required(),
      supportBrand: Joi.string().required(),
      vatType: Joi.string().required(),
      decimalType: Joi.number()
        .required()
        .example(10.0)
        .min(0),
      getProductType: Joi.string()
        .required()
        .example("ASD ASD"),
      customerType: Joi.string().required(),
      tranferFee: Joi.number()
        .example(7)
        .required()
        .min(0)
    }),
    headers: Joi.object({
      authorization: Joi.string().required()
    })
  },
  delete: {
    params: Joi.object({
      id: Joi.string()
        .required()
        .example("C0001")
    }),
    headers: Joi.object({
      authorization: Joi.string().required()
    })
  }
};
