import { CustomerControllers } from "./controller";
import * as Hapi from "hapi";
import { API, HTTPVERB } from "./../../constanst";
import { customerValidate } from "./validator";

const customerController = new CustomerControllers();

export const customerRoute: Array<Hapi.ServerRoute> = [
  {
    method: HTTPVERB.POST,
    path: `${API}/customer/create`,
    options: {
      description: "Create Customer",
      validate: {
        payload: customerValidate.create.payload,
        // headers: customerValidate.create.headers,
        options: {
          allowUnknown: true
        }
      },
      tags: ["api", "customer"]
    },
    handler: customerController.create
  },
  {
    method: HTTPVERB.GET,
    path: `${API}/customer`,
    options: {
      description: "Get all",
      tags: ["api", "customer"]
    },
    handler: customerController.getAll
  },
  {
    method: HTTPVERB.GET,
    path: `${API}/customer/{id}`,
    options: {
      description: "get by id",
      validate: {
        params: customerValidate.getOne.params
      },
      tags: ["api", "customer"]
    },
    handler: customerController.getById
  },
  {
    method: HTTPVERB.PATCH,
    path: `${API}/customer/edit/{id}`,
    options: {
      description: "Edit customer",
      validate: {
        params: customerValidate.editByid.params,
        payload: customerValidate.editByid.payload
        // headers: customerValidate.create.headers
      },
      tags: ["api", "customer"],
      handler: customerController.update
    }
  },
  {
    method: HTTPVERB.DELETE,
    path: `${API}/customer/delete/{id}`,
    options: {
      description: "soft delete Customer",
      validate: {
        params: customerValidate.delete.params
        // headers: customerValidate.create.headers
      },
      tags: ["api", "customer"]
    },
    handler: customerController.destroy
  }
];
