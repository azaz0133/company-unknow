// import { controller } from "../../interfaces/controller";
import {
  IRequestCreateCustomer,
  IGetByCusIdRequest
} from "../../interfaces/api/customer";
import * as Hapi from "hapi";
import * as boom from "boom";
import { ECustomerModel } from "./model";
import { REPOSITORY } from "../../constanst";
import { IRequest, IGetRequest } from "./../../interfaces/request";
import { Pagination } from "./../paginate";
import { authorBy } from "../../utils/dbCheck/authorizeBy";
import { IResponse } from "../../interfaces/api/common";
import { saveImage } from "../../utils/image";

export class CustomerControllers {
  getAll = async (
    req: IGetRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { page = 1, perPage = 5 } = req.query;
    const paginate = new Pagination(REPOSITORY.CUSTOMER.ENTITY, []);
    try {
      const results = await paginate.paginate({}, +page, +perPage);
      return h.response({
        status: "ok",
        statusCode: 200,
        message: "get customers data",
        results
      });
    } catch (error) {
      return boom.badImplementation(error);
    }
  };

  getById = async (
    req: IGetByCusIdRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const customerModel = new ECustomerModel();
    const { id } = req.params;
    try {
      const result = await customerModel.findById(id.toUpperCase());
      return h.response({
        statusCode: 200,
        status: "ok",
        message: "Success",
        result
      });
    } catch (error) {
      return boom.notFound(error.message);
    }
  };

  create = async (
    req: IRequestCreateCustomer,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const customerModel = new ECustomerModel();
    const attrs = req.payload;
    const employee = await authorBy(req.headers.authorization);
    attrs["createdBy"] = employee;
    try {
      const result = await customerModel.createItem(attrs);
      return h.response({
        statusCode: 201,
        status: "ok",
        nessage: "Created Customer"
      });
    } catch (error) {
      return boom.badImplementation(error);
    }
  };

  update = async (
    req: IGetByCusIdRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const customerModel = new ECustomerModel();
    const { id } = req.params;
    const attrs = req.payload;
    const employee = await authorBy(req.headers.authorization);
    attrs["updatedBy"] = employee;
    try {
      await customerModel.editItem(
        id.toUpperCase(),
        attrs,
        REPOSITORY.CUSTOMER.PK
      );
      return h.response({
        statusCode: 202,
        status: "ok",
        message: `updated customerId: ${id}`
      });
    } catch (error) {
      return boom.badImplementation(error);
    }
  };

  destroy = async (
    req: IRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { id } = req.params;
    const customerModel = new ECustomerModel();
    const employee = await authorBy(req.headers.authorization);
    const deleteBy = employee;
    try {
      await customerModel.destroy(
        id.toUpperCase(),
        REPOSITORY.CUSTOMER.PK,
        deleteBy
      );
      return h.response({
        statusCode: 202,
        status: "ok",
        message: `deleted customerId : ${id}`
      });
    } catch (err) {
      return boom.badImplementation(err);
    }
  };
}
