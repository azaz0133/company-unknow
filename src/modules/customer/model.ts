import { Models } from "./../model";
import { IModel } from "../../interfaces/model";
import { REPOSITORY } from "../../constanst";
import { handlerInformationAttrs } from "../../utils/information";
import { editById } from "../../utils/dbCheck/editByid";
import { primaryKey } from "../../utils/dbCheck/generatePk";


export class ECustomerModel extends Models implements IModel {
  constructor() {
    super(REPOSITORY.CUSTOMER.ENTITY, ["_author.createdBy"]);
  }

  createItem = (attrs: any): Promise<any> =>
    new Promise(async (resolve, reject) => {
      let insertData = {};
      handlerInformationAttrs(insertData, attrs, {
        firstName: "firstName",
        lastName: "lastName",
        address: "address",
        phoneNumber: "phoneNumber",
        tin: "tin",
        detail: "detail",
        organization: "organization",
        email: "email",
        customerType: "customerType"
      });

      const { createdBy } = attrs;
      try {
        let customerId = await primaryKey(
          "C",
          REPOSITORY.CUSTOMER.PK,
          5,
          this.collection()
        );
        const value = {
          customerId,
          ...insertData,
          _author: {
            createdBy
          }
        };
        try {
          const result = await this.create(value, this.collection());
          resolve(result);
        } catch (error) {
          reject(`ERROR when create customer ${error.toString()}`);
        }
      } catch (error) {
        reject(`error when generate primary key ${error.toString()}`);
      }
    });

  editItem = (id: string, attrs: any, pk: string) =>
    new Promise(
      async (resolve, reject): Promise<any> => {
        const {
          firstName,
          lastName,
          address,
          phoneNumber,
          tin,
          detail,
          postcode,
          updatedBy
        } = attrs;

        const attr = {
          ...attrs,
          _: {
            firstName,
            lastName,
            address,
            phoneNumber,
            tin,
            detail,
            postcode
          },
          _author: {
            updatedBy
          }
        };
        try {
          await editById(attr, id, pk, this.collection());
          resolve("edited");
        } catch (error) {
          reject(`error when editing ` + error.toString());
        }
      }
    )
}
