import { TrickModel } from "./model";
import { IRequest } from "../../interfaces/request";
import { IResponse } from "../../interfaces/api/common";
import * as boom from 'boom'

export class TrickController {
  getNameForSearchEngine = async (req: IRequest, h: IResponse) => {
    const { query: { keyword } }: any = req
    try {
      const result = await new TrickModel().getNameForSuggestionEngine(keyword)
      return h.response({
        result
      });

    } catch (error) {
      return boom.badRequest(error.message)
    }
  };
}
