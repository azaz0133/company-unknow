import { ServerRoute } from "hapi";
import { HTTPVERB, API } from "../../constanst";
import { TrickController } from "./controller";

const trick = new TrickController();

export const trickRoute: ServerRoute[] = [
  {
    method: HTTPVERB.GET,
    path: `${API}/suggestion`,
    handler: trick.getNameForSearchEngine
  }
];
