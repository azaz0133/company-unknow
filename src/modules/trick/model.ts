import { getConnection } from "typeorm";
import { REPOSITORY } from "../../constanst";

export class TrickModel {

  getNameForSuggestionEngine = (keyword: string) => new Promise(async (resolve, reject) => {
    try {
      const result = await getConnection().query(`
         select _Firstname as firstname, _Lastname as lastname, customerId, initialsName
         from e_customer
         where CUseflag = true AND (_Firstname LIKE  '${keyword}%' OR initialsName LIKE '${keyword}%')
         order by _Firstname desc
         LIMIT 5;
      `)
      resolve(result);

    } catch (error) {
      reject(error)
    }
  });
}
