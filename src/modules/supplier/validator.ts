import * as Joi from "joi";

export const supplierValidate = {
  create: {
    payload: Joi.object({
      prefixName: Joi.string().required(),
      firstName: Joi.string()
        .example("SOmchai")
        .required(),
      lastName: Joi.string()
        .example("SSS")
        .required(),
      phoneNumber: Joi.string()
        .example("081352228")
        .required(),
      email: Joi.string()
        .email()
        .example("socmc@gmail.com")
        .required(),
      address: Joi.string().required(),
      postcode: Joi.string()
        .required()
        .example("10021")
        .max(5)
    })
  },
  update: {
    params: Joi.object({
      id: Joi.string().required()
    }),
    payload: Joi.object({
      prefixName: Joi.string().required(),
      branch: Joi.string().required(),
      contact: Joi.string().required(),
      longtitde: Joi.number().required(),
      latitude: Joi.number().required(),
      amountCredit: Joi.number().required(),
      conditionPayment: Joi.string().required(),
      calculatingGas: Joi.string().required(),
      determinationPrice: Joi.string().required(),
      supportBrand: Joi.string().required(),
      vatType: Joi.string().required(),
      decimalType: Joi.number()
        .required()
        .min(0),
      getProductType: Joi.string().required(),
      customerType: Joi.string().required(),
      tranferFee: Joi.number()
        .required()
        .min(0),
      firstName: Joi.string()
        .example("SOmchai")
        .required(),
      lastName: Joi.string()
        .example("SSS")
        .required(),
      phoneNumber: Joi.string()
        .example("081352228")
        .required(),
      email: Joi.string()
        .email()
        .example("socmc@gmail.com")
        .required(),
      position: Joi.string()
        .example("aaa")
        .required(),
      address: Joi.string().required(),
      status: Joi.string().required(),
      tin: Joi.string().required(),
      detail: Joi.string().required(),
      faxNumber: Joi.string().required(),
      cashCredit: Joi.number().required(),
      postcode: Joi.string().required()
    })
  }
};
