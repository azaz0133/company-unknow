import { REPOSITORY } from "../../constanst";
import { Models } from "../model";
import { primaryKey } from "../../utils/dbCheck/generatePk";
import { editById } from "../../utils/dbCheck/editByid";
import { handlerInformationAttrs } from "../../utils/information";

export class ESupplierModel extends Models {
  constructor() {
    super(REPOSITORY.SUPPLIER.ENTITY, []);
  }

  createItem = (attrs: any): Promise<any> =>
    new Promise(async (resolve, reject) => {
      const {
        firstName,
        lastName,
        address,
        postcode,
        phoneNumber,
        prefixName,
        email
      } = attrs;
      const supplierId = await primaryKey(
        "S",
        REPOSITORY.SUPPLIER.PK,
        5,
        this.collection()
      );
      const { createdBy } = attrs;
      attrs[REPOSITORY.SUPPLIER.PK] = supplierId;
      const value = {
        supplierId,
        ...attrs,
        prefixName,
        _: {
          firstName,
          lastName,
          address,
          postcode,
          phoneNumber,
          email,
        },
        _author: {
          createdBy
        }
      };
      try {
        const result = await this.create(value, this.collection());
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });

  editItem = async (id: string, attrs: any, pk: string): Promise<any> =>
    new Promise(async (resolve, reject) => {
      let insertData = {};
      const { updatedBy } = attrs;
      delete insertData["updatedBy"];
      const value = {
        ...attrs, 
        _:{
          ...attrs
        },   
        _author: {
          updatedBy
        }
      };
      try {
        const result = await editById(value, id, pk, this.collection());
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
}
