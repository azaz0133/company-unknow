import { ServerRoute } from "hapi";
import { API, HTTPVERB } from "../../constanst";
import { SupplierControllers } from "./controller";
import { supplierValidate } from "./validator";

const supplierControllers = new SupplierControllers();

export const supplierRoutes: Array<ServerRoute> = [
  {
    path: `${API}/supplier`,
    method: HTTPVERB.GET,
    options: {
      tags: ["api", "supplier"],
      description: "Supplierr"
    },
    handler: supplierControllers.getAll
  },
  {
    path: `${API}/supplier/{id}`,
    method: HTTPVERB.GET,
    options: {
      tags: ["api", "supplier"],
      validate: {
        params: supplierValidate.update.params
      }
    },
    handler: supplierControllers.getById
  },
  {
    path: `${API}/supplier/edit/{id}`,
    method: HTTPVERB.PATCH,
    options: {
      tags: ["api", "supplier"],
      validate: {
        params: supplierValidate.update.params,
        payload: supplierValidate.update.payload
      }
    },
    handler: supplierControllers.update
  },
  {
    path: `${API}/supplier/create`,
    method: HTTPVERB.POST,
    options: {
      tags: ["api", "supplier"],
      description: "Supplierr",
      validate: {
        payload: supplierValidate.create.payload
      }
    },
    handler: supplierControllers.create
  },
  {
    path: `${API}/supplier/delete/{id}`,
    method: HTTPVERB.DELETE,
    options: {
      tags: ["api", "supplier"],
      validate: {
        params: supplierValidate.update.params
      }
    },
    handler: supplierControllers.destroy
  }
];
