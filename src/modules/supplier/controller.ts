import { IRequest } from "../../interfaces/request";
import { IResponse } from "../../interfaces/api/common";
import { ResponseObject } from "hapi";
import * as boom from "boom";
import { ESupplierModel } from "./model";
import { authorBy } from "../../utils/dbCheck/authorizeBy";
import { Pagination } from "../paginate";
import { REPOSITORY } from "../../constanst";

export class SupplierControllers {
  create = async (
    req: IRequest,
    h: IResponse
  ): Promise<ResponseObject | boom> => {
    const attrs: any = req.payload;
    const supplier = new ESupplierModel();
    const employee = await authorBy(req.headers.authorization);
    attrs["createdBy"] = employee;
    try {
      const result = await supplier.createItem(attrs);
      return h.response({
        statusCode: 201,
        status: "created",
        message: "created supplier",
        result
      });
    } catch (error) {
      return boom.badGateway(error);
    }
  };

  update = async (
    req: IRequest,
    h: IResponse
  ): Promise<ResponseObject | boom> => {
    const { id } = req.params;
    const supplierModel = new ESupplierModel();
    const attrs = req.payload;
    const employee = await authorBy(req.headers.authorization);
    attrs["updatedBy"] = employee;

    try {
      await supplierModel.editItem(id, attrs, REPOSITORY.SUPPLIER.PK);
      return h.response({
        statusCode: 202,
        status: "updated",
        message: "updated supplier : " + id
      });
    } catch (error) {
      return boom.badRequest(error);
    }
  };

  destroy = async (
    req: IRequest,
    h: IResponse
  ): Promise<ResponseObject | boom> => {
    const { id } = req.params;
    const supplierModel = new ESupplierModel();
    const employee = await authorBy(req.headers.authorization);
    const deleteBy = employee;
    try {
      await supplierModel.destroy(
        id.toUpperCase(),
        REPOSITORY.SUPPLIER.PK,
        deleteBy
      );
      return h.response({
        statusCode: 202,
        status: "ok",
        message: `deleted suppier : ${id}`
      });
    } catch (error) {
      return boom.badRequest(error);
    }
  };

  getAll = async (
    req: IRequest,
    h: IResponse
  ): Promise<ResponseObject | boom> => {
    const { page = 1, perPage = 5 } = req.params;
    try {
      const results = await new Pagination(
        REPOSITORY.SUPPLIER.ENTITY,
        []
      ).paginate({}, +page, +perPage);
      return h.response({
        statusCode: 200,
        status: "ok",
        message: "Get all supllier",
        results
      });
    } catch (error) {
      return boom.notFound(error);
    }
  };

  getById = async (
    req: IRequest,
    h: IResponse
  ): Promise<ResponseObject | boom> => {
    const { id } = req.params;
    try {
      const result = await new ESupplierModel().findById(id.toUpperCase());
      return h.response({
        statusCode: 200,
        status: "ok",
        message: " get supplier id",
        result
      });
    } catch (error) {
      return boom.notFound(error.message);
    }
  };
}
