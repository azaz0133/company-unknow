import { Collection } from "./collection";
import { getConnection } from "typeorm";

export class Finder extends Collection {
  public relation:string[] = []
  constructor(key, relations,relation?) {
    super(key, relations);
    if(relation) this.relation = relation
  }

  public where(conditions): Promise<any> {
    return new Promise((resolve, reject) => {
      this.findAll()
        .then(async data => {
          resolve(
            Object.keys(conditions).reduce((results: any, key: string) => {
              results.filter(item => item[key] == conditions[key]);
            }, data)
          );
        })
        .catch((err: Error) => {
          reject(err);
        });
    });
  }

  protected async findAll(): Promise<any> {
    const loadRepository = await getConnection()
      .getRepository(this.collection())
      .find({
        relations: [
          ...this.relations,
          ...this.relation,
          "_author.createdBy",
          "_author.updatedBy",
          "_author.deletedBy"
        ],
        where: {
          _c: {
            useFlag: true
          }
        }
      });

    return new Promise((resolve, reject) => {
      if (loadRepository === undefined)
        reject({
          message: "have no information in database"
        });
      resolve({
        [this.keyNameOfEntity]: loadRepository
      });
    });
  }

  public findById = (id: string): Promise<any> =>
    new Promise(async (resolve, reject) => {
      try {
        const loadRepositoryWithId = await getConnection()
          .getRepository(this.collection())
          .findOne(id, {
            relations: this.relations,
            where: {
              _c: {
                useFlag: true
              }
            }
          });
        if (loadRepositoryWithId !== undefined) {
          resolve({
            [this.keyNameOfEntity]: loadRepositoryWithId
          });
        } else {
          reject({
            message: `not found ${
              this.keyNameOfEntity
            }  primary key : ${id} in database`
          });
        }
      } catch (error) {
        reject(error);
      }
    });
}
