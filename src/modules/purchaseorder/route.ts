import * as Hapi from "hapi";
import { HTTPVERB, API } from "./../../constanst";
import { PurchaseOrderController } from "./controller";
import { purchaseOrderValidate } from "./validator";
import * as Joi from "joi";

const purchaseControl = new PurchaseOrderController();

export const purchaseOrderRoute: Array<Hapi.ServerRoute> = [
  {
    method: HTTPVERB.GET,
    path: `${API}/purchaseorder`,
    options: {
      description: "Get All data",
      tags: ["api", "purchaseOrder"],
      notes: "noo"
    },
    handler: purchaseControl.getAll
  },
  {
    method: HTTPVERB.GET,
    path: `${API}/purchaseorder/{id}`,
    options: {
      description: "Get by id",
      tags: ["api", "purchaseOrder"],
      validate: purchaseOrderValidate.getById
    },
    handler: purchaseControl.getById
  },
  {
    method: HTTPVERB.POST,
    path: `${API}/purchaseorder/create`,
    options: {
      // auth: "jwt",
      description: "create purchaseOrder",
      tags: ["api", "purchaseOrder"],
      validate: purchaseOrderValidate.create
    },
    handler: purchaseControl.create
  },
  {
    method: HTTPVERB.PATCH,
    path: `${API}/purchaseorder/edit/{id}`,
    options: {
      auth: "jwt",
      description: "edit po",
      tags: ["api", "purchaseOrder"],
      validate: purchaseOrderValidate.update
    },
    handler: purchaseControl.update
  },
  {
    method: HTTPVERB.DELETE,
    path: `${API}/purchaseorder/delete/{id}`,
    options: {
      auth: "jwt",
      description: "deleted po",
      tags: ["api", "purchaseOrder"],
      validate: purchaseOrderValidate.destroy
    },
    handler: purchaseControl.destroy
  },
  {
    method: HTTPVERB.GET,
    path: `${API}/print/pdf/{productId}`,
    options: {
      validate: {
        params: {
          productId: Joi.string().required()
        }
      },
      // validate: purchaseOrderValidate.getPdf,
      tags: ["api", "purchaseOrder"]
    },
    handler: purchaseControl.getPurchaseOrderPDF
  },
  {
    method: HTTPVERB.GET,
    path: `/printpdf/{purchaseOrderId}`,
    options: {
      // validate: purchaseOrderValidate.getPdf,
      // tags: ["api", "purchaseOrder"]
    },
    handler: purchaseControl.printPdf
  }
];
