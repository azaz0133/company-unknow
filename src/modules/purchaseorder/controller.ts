import * as Hapi from "hapi";
import { IResponse } from "../../interfaces/api/common";
import * as boom from "boom";
import { IRequest } from "../../interfaces/request";
import { EPurchaseOrderModel } from "./model";
import { REPOSITORY } from "../../constanst";
import { Pagination } from "../paginate";
import { IRequestId } from "./../../interfaces/api/common";
import { authorBy } from "../../utils/dbCheck/authorizeBy";

export class PurchaseOrderController {
  create = async (
    req: IRequest,
    h: IResponse
  ): Promise<Hapi.ResponseObject | boom> => {
    const attrs: any = req.payload;
    const employee = await authorBy(req.headers.authorization);
    const po = new EPurchaseOrderModel();
    attrs["createdBy"] = employee;
    try {
      const purchaseOrderId = await po.createItem(attrs);
      return h.response({
        statusCode: 201,
        status: "ok",
        meesage: "created",
        purchaseOrderId
      });
    } catch (error) {
      return boom.badRequest(error);
    }
  };

  update = async (
    req: IRequest,
    h: IResponse
  ): Promise<Hapi.ResponseObject | boom> => {
    const { id } = req.params;
    const purchaseOrder = new EPurchaseOrderModel();
    const attrs = req.payload;
    const employee = await authorBy(req.headers.authorization);
    attrs["updatedBy"] = employee;
    try {
      await purchaseOrder.editItem(
        id.toUpperCase(),
        attrs,
        REPOSITORY.PRODUCTORDER.PK
      );

      return h
        .response({
          statusCode: 202,
          status: "updated",
          message: "updated po already"
        })
        .code(202);
    } catch (error) {
      return boom.badRequest(error);
    }
  };

  getAll = async (
    req: IRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { page = 1, perPage = 5 } = req.params;
    try {
      // const results = await new Pagination(
      //   REPOSITORY.PRODUCTORDER.ENTITY,
      //   ["purchaseOrderInPO"],
      // ).paginate({}, +page, +perPage);
      const results =  await (new EPurchaseOrderModel().getAllPurchaseOrder())

      return h.response({
        statusCode: 200,
        status: "ok",
        results
      });
    } catch (error) {
      return boom.badRequest(error);
    }
  };

  getById = async (
    req: IRequestId,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { id } = req.params;
    try {
      const result = await new EPurchaseOrderModel().getPurchaseOrder(id)
      return h.response({
        statusCode: 200,
        status: "ok",
        result
      });
    } catch (error) {
      return boom.notFound(error);
    }
  };

  destroy = async (
    req: IRequestId,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { id } = req.params;
    const productOrderModel = new EPurchaseOrderModel();
    const employee = await authorBy(req.headers.authorization);
    const deleteBy = employee;
    try {
      await productOrderModel.destroy(
        id.toUpperCase(),
        REPOSITORY.PRODUCTORDER.PK,
        deleteBy
      );
      return h
        .response({
          statusCode: 200,
          status: "deleted",
          message: "delete purchaseOrder"
        })
        .code(200);
    } catch (error) {
      return boom.badRequest(error);
    }
  };

  printPdf = async (
    req: IRequestId,
    h: any
  ): Promise<Hapi.ResponseObject | boom> => {
    const { purchaseOrderId }: any = req.params;
    const po = new EPurchaseOrderModel();
    const pdf = await po.createPurchaseOrderPDF(purchaseOrderId);
    return h.file(pdf);
  };
  getPurchaseOrderPDF = async (
    req: IRequestId,
    h: any
  ): Promise<Hapi.ResponseObject | boom> => {
    const { productId }: any = req.params;
    return h.view("printpdf.html", {
      productId
    });
  };
}
