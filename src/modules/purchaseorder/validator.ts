import * as Joi from "joi";

export const purchaseOrderValidate = {
  getById: {
    params: Joi.object({
      id: Joi.string()
        .required()
        .example("PO0000001")
    })
  },

  update: {
    params: Joi.object({
      id: Joi.string()
        .required()
        .example("PO0000001")
    }),
    payload: Joi.object({
      subTotal: Joi.number()
        .min(0)
        .optional()
        .example(5300),
      vat: Joi.number()
        .min(0)
        .optional()
        .example(7),
      grandTotal: Joi.number()
        .optional()
        .example(2000),
      transportStatus: Joi.string().required(),
      paymentStatus: Joi.string().required(),
      arrears: Joi.number().required(),
      paymentType: Joi.string().required(),
      billType: Joi.string().required()
    })
  },

  destroy: {
    params: Joi.object({
      id: Joi.string()
        .required()
        .example("P0000001")
    })
  },

  create: {
    payload: Joi.object({
      vat: Joi.number()
        .optional()
        .default(7.0),
      products: Joi.array()
        .required()
        .example(["P0000002", "P0000001", "P0000004"]),
      customerId: Joi.string()
        .required()
        .example("C0001"),
      discount: Joi.number()
        .optional()
        .default(0),
      transportStatus: Joi.string().required(),
      paymentStatus: Joi.string().required(),
      arrears: Joi.number()
        .required()
        .min(0),
      paymentType: Joi.string().required(),
      billType: Joi.string().required()
    })
  },

  getPdf: {
    params: Joi.object({
      id: Joi.string()
        .required()
        .example("PO0000001")
    })
  }
};
