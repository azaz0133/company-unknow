import { Models } from "../model";
import { IModel } from "../../interfaces/model";
import { primaryKey } from "../../utils/dbCheck/generatePk";
import { REPOSITORY } from "../../constanst";
import { editById } from "../../utils/dbCheck/editByid";
import * as moment from "moment";
import { DiscountModel } from "../discount/byCustomer/model";
import { EDiscountProductModel } from "../discount/byProduct/model";
import { getConnection } from "typeorm";
import { EProductInPOModel } from "../productInPo/model";
import { PurchaseOrderPDF } from "../../entity/pdfMaker/purchaseOrder";
import { createPurchaseOrderPDF } from "../../utils/pdfMaker/purchaseorder";

export class EPurchaseOrderModel extends Models implements IModel {
  constructor() {
    super(REPOSITORY.PRODUCTORDER.ENTITY, ["fk1"]);
  }

  createItem = (attrs: any): Promise<any> =>
    new Promise(async (resolve, reject) => {
      const { customerId, discount, products } = attrs;
      const purchaseOrderId = await primaryKey(
        "PO",
        REPOSITORY.PRODUCTORDER.PK,
        8,
        this.collection()
      );
      attrs["purchaseOrderId"] = purchaseOrderId;
      const purchaseOrder = new EPurchaseOrderModel();
      const date = new Date();
      const month = moment(date).format("MMM");
      const year = moment(date).format("YYYY");
      const discountModel = new DiscountModel();
      const checkDup = {};
      products.map(data => {
        if (checkDup[data] !== undefined) {
          checkDup[data]++;
        } else {
          checkDup[data] = 1;
        }
      });
      try {
        const customerDiscount = (await discountModel.getBycustomerId(
          customerId.toUpperCase()
        ))[0];
        let discountForcustomer = 0;
        if (customerDiscount["discount"].length != 0) {
          const filter = customerDiscount["discount"].filter(
            data => data["month"] == month && data["year"] == year
          )[0];
          discountForcustomer = filter["discount"];
          if (discount > discountForcustomer) {
            return reject(
              "wrong discount discount is greater than month discount"
            );
          }
          const updatedDiscount = new DiscountModel().editDiscount;
          filter["discount"] = discountForcustomer - discount;
          try {
            await updatedDiscount(filter["id"], filter);
          } catch (error) {
            return reject(error);
          }
        }

        const productDiscount = new EDiscountProductModel().getBycustomerId;
        try {
          const disProduct: any = await productDiscount(customerId);
          const findProductDiscount = [];
          let totaldiscountProduct = 0;

          if (disProduct !== 404) {
            disProduct.map(data => {
              Object.keys(checkDup).map(productId => {
                if (data["product"]["productId"] == productId)
                  findProductDiscount.push({
                    productId,
                    discount: data["changeByCustomer"]
                  });
              });
            });
            findProductDiscount.map(data => {
              Object.keys(checkDup).map(key => {
                if (data["productId"] == key) {
                  totaldiscountProduct =
                    totaldiscountProduct + checkDup[key] * data["discount"];
                }
              });
            });
          }
          const finalDiscount = discount + totaldiscountProduct;

          try {
            const findProducts = await getConnection()
              .getRepository(REPOSITORY.PRODUCT.ENTITY)
              .findByIds(attrs["products"]);
            let prices: any = 0;
            let products = [];
            for (let i = 0; i < findProducts.length; i++) {
              prices +=
                parseFloat(findProducts[i]["finalPrice"]) *
                checkDup[findProducts[i]["productId"]];
              products.push(findProducts[i]);
            }
            const finalPrice = prices.toFixed(2) - finalDiscount;
            attrs["grandTotal"] = finalPrice * ((100 + attrs["vat"]) / 100);
            attrs["subTotal"] = finalPrice;
            try {
              const customer = await getConnection()
                .getRepository("ECustomer")
                .findOne(customerId, {
                  where: {
                    _c: {
                      useFlag: true
                    }
                  }
                });
              if (customer === undefined) reject("Not found this Customer");

              attrs["fk1"] = customer;
              try {
                const idPo: any = await purchaseOrder.create(
                  attrs,
                  this.collection()
                );
                const { purchaseOrderId } = idPo;
                const valuesInPo = [];
                for (let i = 0; i < products.length; i++) {
                  valuesInPo.push({
                    fk1: purchaseOrderId,
                    fk2: products[i],
                    total: checkDup[products[i]["productId"]]
                  });
                }
                const productOrder = new EProductInPOModel();
                try {
                  await productOrder.create(valuesInPo);
                  resolve(purchaseOrderId);
                } catch (error) {
                  console.log("1");
                  reject(error);
                }
              } catch (error) {
                console.log("2");

                reject(error);
              }
            } catch (error) {
              console.log("3");

              reject(error);
            }
          } catch (error) {
            console.log("4");

            reject(error);
          }
        } catch (error) {
          console.log("5");

          reject(error);
        }
      } catch (error) {
        console.log("6");

        reject(error);
      }
      const { createdBy } = attrs;
      attrs[REPOSITORY.PRODUCTORDER.PK] = purchaseOrderId;

      const value = {
        ...attrs,
        _author: {
          createdBy
        }
      };
      try {
        resolve(await this.create(value, this.collection()));
      } catch (error) {
        reject(error);
      }
      try {
      } catch (error) {
        resolve(error);
      }
    });

  getAllPurchaseOrder = () =>
    new Promise(async (resolve, reject) => {
      const purchaseOrders = await getConnection()
        .getRepository(REPOSITORY.PRODUCTORDER.ENTITY)
        .createQueryBuilder("po")
        .leftJoinAndSelect("po.purchaseOrderInPO", "product")
        .leftJoinAndSelect("product.fk2", "products")
        .leftJoinAndSelect("po.fk1", "customer")
        .getMany();
      let saveProduct = [];
      purchaseOrders.map(data => {
        saveProduct.push(data["purchaseOrderInPO"]);
      });

      saveProduct = saveProduct.map(data =>
        data.map(d => {
          delete d["fk2"]["_c"];
          return {
            ...d["fk2"],
            total: d["total"]
          };
        })
      );
      let results = [];
      purchaseOrders.map((data, i) => {
        delete data["fk1"]["_c"];
        results.push({
          purchaseOrderId: data["purchaseOrderId"],
          subTotal: data["subTotal"],
          vat: data["vat"],
          grandTotal: data["grandTotal"],
          arrears: data["arrears"],
          paymentStatus: data["paymentStatus"],
          transportStatus: data["transportStatus"],
          billType: data["billType"],
          paymentType: data["paymentType"],
          customer: {
            ...data["fk1"]
          },
          products: saveProduct[i]
        });
      });
      resolve(results);
    });

  getPurchaseOrder = (purchaseOrderId: string) =>
    new Promise(async (resolve, reject) => {
      let purchaseOrder = await getConnection()
        .getRepository(REPOSITORY.PRODUCTORDER.ENTITY)
        .createQueryBuilder("po")
        .leftJoinAndSelect("po.purchaseOrderInPO", "product")
        .leftJoinAndSelect("product.fk2", "products")
        .leftJoinAndSelect("po.fk1", "customer")
        .where("po.purchaseOrderId = :purchaseOrderId", {
          purchaseOrderId
        })
        .getOne();
      if (purchaseOrder === undefined) {
        reject({
          message: "not found"
        })
      } else {
        const customer = purchaseOrder["fk1"];
        delete customer["_c"];
        delete purchaseOrder["fk1"];
        const product = purchaseOrder["purchaseOrderInPO"].map(data => {
          delete data["fk2"]["_c"];
          return {
            ...data["fk2"],
            total: data["total"]
          };
        });
        delete purchaseOrder["purchaseOrderInPO"];
        resolve({
          ...purchaseOrder,
          customer,
          product
        });
      }
    });

  editItem = (id: string, attrs: any, pk: string) =>
    new Promise(
      async (resolve, reject): Promise<any> => {
        const { updatedBy } = attrs;
        delete attrs["updatedBy"];
        const value = {
          ...attrs,
          _author: {
            updatedBy
          }
        };
        try {
          await editById(value, id, pk, this.collection());
          resolve("edited");
        } catch (error) {
          reject(error);
        }
      }
    );

  getPurchaseOrderByCustomerId = (customerId: string) =>
    new Promise((resolve, reject) => {
      /* wating for process */
    });

  createPurchaseOrderPDF = (purchaseOrderId: string) =>
    new Promise(async (resolve, reject) => {
      const products = new EProductInPOModel().get;
      let productList: any = await products(purchaseOrderId);
      const customer = (await this.findById(purchaseOrderId))[
        REPOSITORY.PRODUCTORDER.ENTITY
      ]["fk1"];
      let productArr = [];
      let c = {};
      productList.filter(data => {
        Object.keys(data).filter(key => {
          delete data["fk2"]["_c"];
          if (c[data["fk2"]["productId"]] === undefined) {
            productArr.push({
              ...data["fk2"],
              quantity: data["total"],
              amount: (data["fk2"]["finalPrice"] * data["total"]).toFixed(2)
            });
            c[data["fk2"]["productId"]] = data["fk2"]["productId"];
          }
        });
      });
      const purchaseOrderDetail = productList[0]["fk1"];
      const value = this.detailPreorder(
        customer,
        purchaseOrderDetail,
        productArr
      );
      try {
        const path = await createPurchaseOrderPDF(value);
        resolve(path);
      } catch (error) {
        reject(error);
      }
    });

  detailPreorder = (customer, purchaseOrderDetail, productArr) => {
    const value: PurchaseOrderPDF = {
      customer: {
        name: customer._["firstName"] + "  " + customer._["lastName"],
        id: customer["customerId"],
        address: customer._["address"]
      },
      purchaseOrder: {
        id: purchaseOrderDetail["purchaseOrderId"],
        withoutVat: purchaseOrderDetail["subTotal"],
        amount: parseFloat(purchaseOrderDetail["grandTotal"]).toFixed(2),
        vat: purchaseOrderDetail["vat"]
      },
      products: productArr
    };

    return value;
  };
}
