import { API } from "./../../constanst";
import { SessionController } from "./controller";
import * as Joi from "joi";
import * as Hapi from "hapi";
import { sessionvalidate } from "./validator";

const sessionController = new SessionController();
export const sessionRoute: Array<Hapi.ServerRoute> = [
  {
    method: "POST",
    path: `${API}/session/login`,
    options: {
      validate: {
        payload: sessionvalidate.login.payload
      },
      tags: ["api", "session"],
      notes: "use this api for login by username and password",
      plugins: {
        "hapi-swagger": {
          responses: {
            "201": {
              description: "login success",
              schema: Joi.object({
                username: Joi.string().required(),
                password: Joi.string().required()
              })
            }
          }
        }
      }
    },
    handler: sessionController.login
  },
  {
    method: "POST",
    path: `${API}/session/register`,
    options: {
      validate: {
        payload: sessionvalidate.register.payload
      },
      tags: ["api", "session"]
    },
    handler: sessionController.register
  }
];
