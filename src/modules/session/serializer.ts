export const payloadEmployee = payload => {
  const { employeeId, username, roles } = payload;

  return {
    employeeId,
    roles,
    username
  };
};
