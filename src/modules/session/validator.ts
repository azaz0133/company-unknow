import * as Joi from "joi";

export const sessionvalidate = {
  register: {
    payload: Joi.object({
      username: Joi.string()
        .required()
        .example("azaaz0133"),
      password: Joi.string()
        .required()
        .example("1251055"),
      firstName: Joi.string()
        .required()
        .example("Peecha"),
      lastName: Joi.string()
        .required()
        .example("suksee")
    })
  },
  login: {
    payload: {
      username: Joi.string()
        .max(30)
        .required()
        .example("azaz0133"),
      password: Joi.string()
        .required()
        .example("081assda84")
    }
  }
};
