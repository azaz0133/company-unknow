import { ISessionRequest } from "./../../interfaces/api/session";
import { IRequestEmpRegister } from "../../interfaces/api/employee";
import * as Hapi from "hapi";
import { EmployeeModel } from "../employee/model";
import * as boom from "boom";
import { payloadEmployee } from "./serializer";

const userModel = new EmployeeModel();

export class SessionController {
  login = async (req: ISessionRequest, h: Hapi.ResponseToolkit) => {
    const { username, password } = req.payload;
    try {
      const userData = await userModel.findUserByUsername(username);
      try {
        const isValid: boolean = (await userModel.verifyToken(
          userData["result"][0],
          password
        ))["result"];

        if (isValid) {
          const payload = payloadEmployee(userData["result"][0]);
          return h
            .response({
              statusCode: 201,
              status: "ok",
              message: "login success",
              Authorization: `Bearer ${userModel.generateToken(payload)}`,
              employee: payload
            })
            .header(
              "Authorization",
              `Bearer ${userModel.generateToken(payload)}`
            )
            .code(201);
        } else {
          return boom.unauthorized("login unsuccess invalid credential");
        }
      } catch (error) {
        return boom.badImplementation(error);
      }
    } catch (error) {
      return boom.badImplementation(error);
    }
  };

  logout = () => {
    /* waiting devlopment ... */
  };
  register = async (req: IRequestEmpRegister, h: Hapi.ResponseToolkit) => {
    const attrs = req.payload;
    try {
      await userModel.createItem(attrs);
      return h.response({
        statusCode: 201,
        status: "ok",
        message: "Registered Success"
      });
    } catch (err) {
      return boom.badRequest(err);
    }
  };
}
