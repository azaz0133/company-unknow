import * as Hapi from "hapi";
import { HTTPVERB, API } from "../../constanst";
import { ProductOrderController } from "./controller";

const controller = new ProductOrderController();

export const productOrderRoute: Array<Hapi.ServerRoute> = [
  {
    path: `${API}/productorder/{id}`,
    method: HTTPVERB.GET,
    handler: controller.getByPurchaseId
  }
];
