import { IRequest } from "../../interfaces/request";
import { IResponse } from "../../interfaces/api/common";
import * as Hapi from "hapi";
import * as boom from "boom";
import { EProductInPOModel } from "./model";

export class ProductOrderController {
  getByPurchaseId = async (
    req: IRequest,
    h: IResponse
  ): Promise<Hapi.ResponseObject | boom> => {
    const { id } = req.params;
    const productOrderModel = new EProductInPOModel();
    const results: any = await productOrderModel.get(id);

    if (results.length != 0) {
      return h.response({
        status: "ok",
        statusCode: 200,
        message: "productorders",
        results
      });
    } else return boom.notFound();
  };
}
