import { IModel } from "../../interfaces/model";
import { getConnection } from "typeorm";

export class EProductInPOModel {
  create = values =>
    new Promise(async (resolve, reject) => {
      try {
        await getConnection()
          .createQueryBuilder()
          .insert()
          .into("EProductInPO")
          .values(values)
          .execute();
        resolve();
      } catch (error) {
        reject(error);
      }
    });

  get = (purchaseOrderId: string) =>
    new Promise(async (resolve, reject) => {
      try {
        const results = await getConnection()
          .getRepository("EProductInPO")
          .createQueryBuilder("eppo")
          .leftJoinAndSelect("eppo.fk1", "purchaseOrders")
          .leftJoinAndSelect("eppo.fk2", "products")
          .where("eppo.fk1 = :purchaseOrderId", {
            purchaseOrderId
          }) 
          .getMany();
        resolve(results);
      } catch (error) {
        console.log(error);
        reject(error);
      }
    });
}
