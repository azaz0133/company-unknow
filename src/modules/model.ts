import "reflect-metadata";
import * as Repository from "../entity";
import { getConnection } from "typeorm";
import { Pagination } from "./paginate";
import { Finder } from "./finder";
import { Crud } from "../interfaces/crud";
import { EProductInPOModel } from "./productInPo/model";

export class Models extends Finder implements Crud {
  constructor(key, relations) {
    super(key, relations);
  }

  collection = (entity?: string) => {
    if (entity) {
      return Repository[entity];
    }
    return Repository[this.keyNameOfEntity];
  };

  create = (value, collection) =>
    new Promise(async (resolve, reject) => {
      try {
        const id = (await getConnection()
          .createQueryBuilder()
          .insert()
          .into(collection)
          .values([value])
          .execute()).identifiers;
        resolve(id[0]);
      } catch (error) {
        reject(error);
      }
    });

  destroy(id: string | number, pk: string, deletedBy: Object): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await getConnection()
          .createQueryBuilder()
          .update(this.collection())
          .set({
            _c: {
              useFlag: false,
              deletedAt: new Date()
            },
            _author: {
              deletedBy
            }
          })
          .where(`${pk} = :pk`, {
            pk: id
          })
          .andWhere("_c.useFlag = true")
          .execute();
        if (result.raw.changedRows === 0) reject("not found");
        resolve("deleted");
      } catch (error) {
        reject(error);
      }
    });
  }
}
