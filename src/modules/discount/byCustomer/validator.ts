import * as Joi from "joi";

export const discountValidate = {
  createDiscountPerMonth: {
    payload: Joi.object({
      customerId: Joi.string().required(),
      date: Joi.date().required(),
      discount: Joi.number()
        .required()
        .max(50000)
        .min(0)
    })
  },
  updateDiscountPerMonth: {
    payload: Joi.object({
      date: Joi.date().required(),
      discount: Joi.number()
        .required()
        .min(0)
        .max(5000)
    }),
    params: Joi.object({
      id: Joi.string().required()
    })
  },
  deleteDiscountPerMonth: {
    params: Joi.object({
      id: Joi.number().required()
    })
  },
  getBycustomerId: {
    params: Joi.object({
      customerId: Joi.string()
        .required()
        .example("C0001")
    })
  }
};
