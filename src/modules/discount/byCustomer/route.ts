import * as Hapi from "hapi";
import * as Joi from "joi";
import { API, HTTPVERB } from "../../../constanst";
import { discountValidate } from "./validator";
import { DiscountPerMonthCOntroller } from "./controller";

const discountPerMonth = new DiscountPerMonthCOntroller();

export const discountRoute: Array<Hapi.ServerRoute> = [
  {
    method: HTTPVERB.GET,
    path: `${API}/discount/customer/{customerId}`,
    options: {
      validate: {
        params: discountValidate.getBycustomerId.params
      },
      tags: ["api", "discountByCustomer"]
    },
    handler: discountPerMonth.getDiscountByCustomerId
  },
  {
    method: HTTPVERB.GET,
    path: `${API}/discount`,
    options: {
      tags: ["api", "discountByCustomer"]
    },
    handler: discountPerMonth.getAll
  },
  {
    method: HTTPVERB.GET,
    path: `${API}/discount/{id}`,
    options: {
      description: "get discount per month by id",
      validate: {
        params: {
          id: Joi.string()
            .min(1)
            .required()
        }
      },
      tags: ["api", "discountByCustomer"]
    },
    handler: discountPerMonth.getByIdDiscount
  },
  {
    method: HTTPVERB.POST,
    path: `${API}/customer/discount/create`,
    options: {
      description: "create discount per month",
      validate: {
        payload: discountValidate.createDiscountPerMonth.payload
      },
      tags: ["api", "discountByCustomer"]
    },
    handler: discountPerMonth.createDiscountPermonth
  },
  {
    method: HTTPVERB.PATCH,
    path: `${API}/customer/discount/edit/{id}`,
    options: {
      validate: {
        payload: discountValidate.updateDiscountPerMonth.payload,
        params: discountValidate.updateDiscountPerMonth.params
      },
      tags: ["api", "discountByCustomer"]
    },
    handler: discountPerMonth.updateDiscountPermonth
  },
  {
    method: HTTPVERB.DELETE,
    path: `${API}/customer/discount/delete/{id}`,
    options: {
      tags: ["api", "discountByCustomer"],
      validate: {
        params: discountValidate.deleteDiscountPerMonth.params
      }
    },
    handler: discountPerMonth.destroyDiscount
  }
];
