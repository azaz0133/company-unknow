import { ECustomerModel } from "../../customer/model";
import { REPOSITORY } from "../../../constanst";
import { Models } from "../../model";
import * as moment from "moment";
import { editById } from "../../../utils/dbCheck/editByid";
import { rejects } from "assert";
import { getConnection } from "typeorm";

export class DiscountModel extends Models {
  constructor() {
    super("ECustomerDiscount", ["customerDiscount"]);
  }

  getBycustomerId = customerId =>
    new Promise(async (resolve, reject) => {
      try {
        const results = await getConnection()
          .getRepository(this.collection("ECustomer"))
          .find({
            relations: ["discount"],
            where: {
              customerId,
              _c: {
                useFlag: true
              }
            }
          });
        if (results.length === 0) {
          reject({
            message: " Not found customerId : " + customerId
          });
        }
        resolve(results);
      } catch (error) {
        rejects(error);
      }
    });

  createCustomerPerMonth = async (attrs: any): Promise<any> =>
    new Promise(async (resolve, reject) => {
      const { customerId, date } = attrs;
      attrs["month"] = moment(date).format("MMM");
      attrs["year"] = moment(date).format("YYYY");
      const customerModel = new ECustomerModel();
      try {
        const find = await customerModel.findById(customerId);
        const customer = find[REPOSITORY.CUSTOMER.ENTITY];
        const value = {
          ...attrs,
          customerDiscount: customer,
          _author: {
            createdBy: attrs["createdBy"]
          }
        };
        try {
          await this.create(value, this.collection("ECustomerDiscount"));
          resolve();
        } catch (error) {
          reject(error);
        }
      } catch (error) {
        reject(error);
      }
    });

  editDiscount = (id: string, attrs: any) =>
    new Promise(async (resolve, reject) => {
      const { date } = attrs;
      attrs["month"] = moment(date).format("MMM");
      attrs["year"] = moment(date).format("YYYY");

      try {
        await editById(attrs, id, "id", this.collection("ECustomerDiscount"));
        resolve();
      } catch (error) {
        reject(error);
      }
    });

  delete = (id: string, deleteBy) =>
    new Promise(async (resolve, reject) => {
      try {
        await this.destroy(id, "id", deleteBy);
        resolve();
      } catch (error) {
        reject(error);
      }
    });
}
