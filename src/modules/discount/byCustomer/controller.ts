import { IRequest } from "../../../interfaces/request";
import * as Hapi from "hapi";
import * as boom from "boom";
import { authorBy } from "../../../utils/dbCheck/authorizeBy";
import { DiscountModel } from "./model";
import { Pagination } from "../../paginate";
import { REPOSITORY } from "../../../constanst";
import { getConnection } from "typeorm";

export class DiscountPerMonthCOntroller {
  getAll = async (
    req: IRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { page = 1, perPage = 5 } = req.params;
    const discountModel = new DiscountModel();
    const paginate = new Pagination(REPOSITORY.DISCOUNT.ENTITY[0], [
      "customerDiscount"
    ]);
    try {
      const results = await paginate.paginate({}, +page, +perPage);
      return h.response({
        statusCode: 200,
        status: "ok",
        message: "find all",
        results
      });
    } catch (error) {
      return boom.badRequest(error);
    }
  };

  getByIdDiscount = async (
    req: IRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { id } = req.params;
    const discountModel = new DiscountModel();

    try {
      const result = await discountModel.findById(id);

      return h.response({
        statusCode: 200,
        status: "ok",
        message: "find discount by id",
        result
      });
    } catch (error) {
      return boom.notFound(error.message);
    }
  };

  getDiscountByCustomerId = async (
    req: IRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { customerId } = req.params;
    const discountModel = new DiscountModel();
    try {
      const results = await discountModel.getBycustomerId(customerId);
      return h.response({
        statusCode: 200,
        status: "ok",
        message: "find by customerid",
        results
      });
    } catch (error) {
      return boom.notFound(error.message);
    }
  };

  createDiscountPermonth = async (
    req: IRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const attrs: any = req.payload;
    const employee = await authorBy(req.headers.authorization);
    attrs["createdBy"] = employee;
    try {
      const discountModel = new DiscountModel();
      await discountModel.createCustomerPerMonth(attrs);
      return h.response({
        statusCode: 201,
        status: "ok",
        message: "create discount"
      });
    } catch (error) {
      return boom.badRequest(error.message);
    }
  };

  updateDiscountPermonth = async (
    req: IRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const attrs: any = req.payload;
    const { id } = req.params;
    const employee = await authorBy(req.headers.authorization);
    attrs["updatedBy"] = employee;
    const discountModel = new DiscountModel();
    try {
      await discountModel.editDiscount(id, attrs);
      return h.response({
        statusCode: 202,
        status: "updated",
        message: "update dicount"
      });
    } catch (error) {
      return boom.badRequest(error);
    }
  };

  destroyDiscount = async (
    req: IRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { id } = req.params;
    const discountModel = new DiscountModel();
    const employee = await authorBy(req.headers.authorization);
    const deleteBy = employee;
    try {
      await discountModel.delete(id, deleteBy);
      return h.response({
        statusCode: 201,
        status: "delete",
        message: "delete"
      });
    } catch (error) {
      return boom.badRequest(error);
    }
  };
}
