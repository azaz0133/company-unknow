import * as Hapi from "hapi";
import { IRequest } from "../../../interfaces/request";
import * as boom from "boom";
import { authorBy } from "../../../utils/dbCheck/authorizeBy";
import { EDiscountProductModel } from "./model";
import { Pagination } from "../../paginate";
import { REPOSITORY } from "../../../constanst";

export class DiscountProductController {
  getAll = async (
    req: IRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { page = 1, perPage = 5 } = req.params;
    const paginate = new Pagination(REPOSITORY.DISCOUNT.ENTITY[1], [
      // "customerDiscount"
    ]);
    try {
      const results = await paginate.paginate({}, +page, +perPage);
      return h.response({
        statusCode: 200,
        status: "ok",
        message: "find all",
        results
      });
    } catch (error) {
      return boom.badRequest(error);
    }
  };

  getById = async (
    req: IRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { id } = req.params;
    const productDiscount = new EDiscountProductModel();
    try {
      const result = await productDiscount.findById(id);
      return h.response({
        statusCode: 200,
        status: "ok",
        message: "find discount by id",
        result
      });
    } catch (error) {
      return boom.notFound(error.message);
    }
  };

  create = async (
    req: IRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const attrs: any = req.payload;
    const employee = await authorBy(req.headers.authorization);
    attrs["createBy"] = employee;
    const ProductDiscount = new EDiscountProductModel();
    try {
      await ProductDiscount.createItem(attrs);
      return h.response({
        statusCode: 201,
        status: "Created",
        message: "create discount "
      });
    } catch (error) {
      return boom.badRequest(error);
    }
  };

  getBycustomerId = async (
    req: IRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const productDiscount = new EDiscountProductModel();
    const { customerId } = req.params;
    try {
      const results = await productDiscount.getBycustomerId(customerId);
      return h.response({
        statusCode: 200,
        status: "ok",
        message: "find by id customer",
        results
      });
    } catch (error) {
      return boom.notFound(error);
    }
  };

  update = async (
    req: IRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const attrs: any = req.payload;
    const { id } = req.params;
    const employee = await authorBy(req.headers.authorization);
    attrs["updatedBy"] = employee;
    const productDiscount = new EDiscountProductModel();
    try {
      await productDiscount.edit(id, attrs);
      return h.response({
        statusCode: 202,
        status: "updated",
        message: "updated discount product"
      });
    } catch (error) {
      return boom.badRequest(error);
    }
  };

  destroy = async (
    req: IRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { id } = req.params;
    const productDiscount = new EDiscountProductModel();
    const employee = await authorBy(req.headers.authorization);
    const deleteBy = employee;
    try {
      await productDiscount.destroy(id, REPOSITORY.DISCOUNT.PK, deleteBy);
      return h.response({
        statusCode: 202,
        status: "ok",
        message: `deleted discout product : ${id}`
      });
    } catch (error) {
      return boom.badRequest(error);
    }
  };
}
