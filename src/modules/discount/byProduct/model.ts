import { Models } from "../../model";
import { getConnection } from "typeorm";
import { REPOSITORY } from "../../../constanst";
import { editById } from "../../../utils/dbCheck/editByid";

export class EDiscountProductModel extends Models {
  constructor() {
    super("ECustomerProductDiscount", ["product", "customers"]);
  }
  createItem = (attrs: any) =>
    new Promise(async (resolve, reject) => {
      const { customerId, productId, changeByCustomer } = attrs;

      try {
        const customer = await getConnection()
          .getRepository(REPOSITORY.CUSTOMER.ENTITY)
          .findOne(customerId, {
            where: {
              _c: {
                useFlag: true
              }
            }
          });
        if (customer === undefined) {
          reject("Not found customer");
        }
        try {
          const product = await getConnection()
            .getRepository(REPOSITORY.PRODUCT.ENTITY)
            .findOne(productId, {
              where: {
                _c: {
                  useFlag: true
                }
              }
            });
          if (product === undefined) {
            reject("Not found product");
          }
          const finalPrice =
            product["finalPrice"]  - changeByCustomer;
          const value = {
            customers: customer,
            product,
            finalPrice,
            changeByCustomer
          };
          try {
            await this.create(value, this.collection());
            resolve();
          } catch (error) {
            reject(error);
          }
        } catch (error) {
          reject(error);
        }
      } catch (error) {
        reject(error);
      }
    });

  getBycustomerId = customerId =>
    new Promise(async (resolve, reject) => {
      try {
        const results = await getConnection()
          .getRepository(REPOSITORY.DISCOUNT.ENTITY[1])
          .find({
            relations: [...this.relations],
            where: {
              customers: customerId,
              _c: {
                useFlag: true
              }
            }
          });
        if (results.length === 0) resolve(404);
        resolve(results);
      } catch (error) {
        reject(error);
      }
    });

  edit = (id, attrs) =>
    new Promise(async (resolve, reject) => {
      const {
        customerId,
        productId,
        changeByPtt,
        changeByCustomer,
        updatedBy
      } = attrs;
      try {
        const customer = await getConnection()
          .getRepository(REPOSITORY.CUSTOMER.ENTITY)
          .findOne(customerId, {
            where: {
              _c: {
                useFlag: true
              }
            }
          });
        if (customer === undefined) {
          reject("Not found customer");
        }
        try {
          const product = await getConnection()
            .getRepository(REPOSITORY.PRODUCT.ENTITY)
            .findOne(productId, {
              where: {
                _c: {
                  useFlag: true
                }
              }
            });
          if (product === undefined) {
            reject("Not found product");
          }
          const finalPrice =
            product["finalPrice"] - changeByPtt - changeByCustomer;
          const value = {
            customers: customer,
            product,
            finalPrice,
            changeByPtt,
            changeByCustomer,
            _author: {
              updatedBy
            }
          };

          try {
            await editById(value, id, "id", this.collection());
            resolve();
          } catch (error) {
            reject(error);
          }
        } catch (error) {
          reject(error);
        }
      } catch (error) {
        reject(error);
      }
    });
}
