import * as Hapi from "hapi";
import { API, HTTPVERB } from "../../../constanst";
import { DiscountProductValidate } from "./validator";
import { DiscountProductController } from "./controller";

const controller = new DiscountProductController();

export const discountProductRoute: Array<Hapi.ServerRoute> = [
  {
    method: HTTPVERB.POST,
    path: `${API}/discount/product/create`,
    options: {
      validate: {
        payload: DiscountProductValidate.create.paylaod
      },
      tags: ["api", "discountByProduct"]
    },
    handler: controller.create
  },
  {
    method: HTTPVERB.GET,
    path: `${API}/discount/product`,
    options: {
      validate: {},
      tags: ["api", "discountByProduct"]
    },
    handler: controller.getAll
  },
  {
    method: HTTPVERB.GET,
    path: `${API}/discount/product/{id}`,
    options: {
      validate: {
        params: DiscountProductValidate.getById.params
      },
      tags: ["api", "discountByProduct"]
    },
    handler: controller.getById
  },
  {
    method: HTTPVERB.GET,
    path: `${API}/discount/product/customer/{customerId}`,
    options: {
      validate: {
        params: DiscountProductValidate.getByCustomerId.params
      },
      tags: ["api", "discountByProduct"]
    },
    handler: controller.getBycustomerId
  },
  {
    method: HTTPVERB.DELETE,
    path: `${API}/discount/product/customer/delete/{id}`,
    options: {
      validate: {
        params: DiscountProductValidate.update.params
      },
      tags: ["api", "discountByProduct"]
    },
    handler: controller.destroy
  }
];
