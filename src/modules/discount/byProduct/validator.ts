import * as Joi from "joi";
import { join } from "path";

export const DiscountProductValidate = {
  create: {
    paylaod: Joi.object({
      customerId: Joi.string()
        .required()
        .example("C0002"),
      productId: Joi.string()
        .required()
        .example("P0000004"),
      changeByCustomer: Joi.number()
        .required()
        .min(0)
        .max(99999)
        .example(50)
    })
  },
  getById: {
    params: Joi.object({
      id: Joi.string()
        .required()
        .example(1)
    })
  },
  getByCustomerId: {
    params: Joi.object({
      customerId: Joi.string()
        .required()
        .example("C0001")
    })
  },
  update: {
    params: {
      id: Joi.number()
        .required()
        .example(1)
    },
    payload: {
      customerId: Joi.string()
        .required()
        .example("C0002"),
      productId: Joi.string()
        .required()
        .example("P0000004"),

      changeByCustomer: Joi.number()
        .required()
        .example(50)
        .min(0)
        .max(9999)
    }
  }
};
