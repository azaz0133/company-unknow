import * as Repository from "../entity";

export class Collection {
  protected keyNameOfEntity: string;
  public relations: Array<string>;

  constructor(key, relations) {
    this.keyNameOfEntity = key;
    this.relations = relations;
  }

  protected collection = () => Repository[this.keyNameOfEntity];
}
