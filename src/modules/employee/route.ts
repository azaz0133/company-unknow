import { employeeValidate } from "./validator";
import { API, HTTPVERB } from "./../../constanst";
import { EmployeeController } from "./controller";
import * as Hapi from "hapi";

const employeeController = new EmployeeController();

export const employeeRoute: Array<Hapi.ServerRoute> = [
  {
    path: `${API}/employee/create`,
    method: HTTPVERB.POST,
    options: {
      validate: {
        payload: employeeValidate.create.payload,
      },
      tags: ['api', "employee"]
    },
    handler: employeeController.create
  },
  {
    method: HTTPVERB.GET,
    path: `${API}/employee`,
    options: {
      // auth: "jwt",
      notes: "get all Employee data",
      tags: ["api", "employee"]
    },
    handler: employeeController.getAll
  },
  {
    method: HTTPVERB.DELETE,
    path: `${API}/employee/delete/{id}`,
    options: {
      description: "Soft Delete Employee",
      validate: {
        params: employeeValidate.delete.params
        // headers: employeeValidate.delete.headers
      },
      tags: ["api", "employee"]
    },
    handler: employeeController.destroy
  },
  {
    method: HTTPVERB.GET,
    path: `${API}/employee/{id}`,
    options: {
      description: "Get employee by id",
      validate: {
        params: employeeValidate.getOne.params
      },
      tags: ["api", "employee"]
    },
    handler: employeeController.getById
  },
  {
    method: HTTPVERB.PATCH,
    path: `${API}/employee/edit/{id}`,
    options: {
      description: "edit employee ",
      validate: {
        params: employeeValidate.editById.params,
        payload: employeeValidate.editById.payload
        // headers: employeeValidate.delete.headers
      },
      tags: ["api", "employee"]
    },
    handler: employeeController.update
  },
  {
    method: HTTPVERB.POST,
    path: `${API}/upload/image/{id}`,
    options: {
      validate: {
        params: {
          id: employeeValidate.upload.params.id
        },
        payload: {
          uri: employeeValidate.upload.payload.uri
        }
      },
      description:"support file only jpg type !",
      tags: ['api', 'employee']
    },
    handler: employeeController.upload
  },
  {
    method: HTTPVERB.GET,
    path: `${API}/employee/profilepicture/{id}`,
    options: {
      validate: {
        params: {
          id: employeeValidate.upload.params.id
        }
      },
      tags: ['api', 'employee']
    },
    handler: employeeController.profilePicture
  }
];
