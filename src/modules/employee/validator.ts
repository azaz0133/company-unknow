import * as Joi from "joi";

export const employeeValidate = {
  delete: {
    params: Joi.object({
      id: Joi.string()
        .required()
        .example("E0001")
    }),
    headers: Joi.object({
      authorization: Joi.string().required()
    })
  },
  getOne: {
    params: Joi.object({
      id: Joi.string()
        .required()
        .example("E0001")
    }),
    headers: Joi.object({
      authorization: Joi.string().required()
    })
  },
  editById: {
    params: Joi.object({
      id: Joi.string()
        .required()
        .example("E0001")
    }),
    payload: Joi.object({
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      address: Joi.string().required(),
      phoneNumber: Joi.string().required(),
      tin: Joi.string().required(),
      detail: Joi.string().required(),
      department: Joi.string().required(),
      prefixName: Joi.string().required(),
      postcode: Joi.string().required(),
      employeeType: Joi.string().required(),
      electricBill: Joi.number()
        .required()
        .min(0),
      waterbill: Joi.number()
        .required()
        .min(0),
      workStatus: Joi.boolean().required(),
      startDate: Joi.date().required(),
      salary: Joi.number()
        .required()
        .min(0),
      manDay: Joi.number()
        .required()
        .min(0),
      overtime: Joi.number()
        .required()
        .min(0),
      sso: Joi.number()
        .required()
        .min(0),
      position: Joi.string().required(),
      email: Joi.string().email().required().example("azaz0133@gmail.com")
    }),
    headers: Joi.object({
      authorization: Joi.string().required()
    })
  },
  create: {
    payload: Joi.object({
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      address: Joi.string().optional(),
      phoneNumber: Joi.string().optional(),
      // tin: Joi.string().optional(),
      // detail: Joi.string().optional(),
      position: Joi.string().required(),
      username: Joi.string().required(),
      password: Joi.string().required(),
      email: Joi.string()
        .email()
        .required()
        .example("got@gmail.com")
    })
  },
  upload: {
    params: {
      id: Joi.string().required().example("E0001")
    },
    payload: {
      uri: Joi.string().uri().required()
    }
  }
};
