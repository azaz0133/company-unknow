import { REPOSITORY } from "./../../constanst";
import { Models } from "./../model";
import "reflect-metadata";
import { getConnection } from "typeorm";
import * as bcrypt from "bcryptjs";
import * as jwt from "jsonwebtoken";
import { IModel } from "../../interfaces/model";
import { handlerInformationAttrs } from "../../utils/information";
import { editById } from "../../utils/dbCheck/editByid";
import { primaryKey } from "../../utils/dbCheck/generatePk";
import { saveImage } from "../../utils/image";
import * as resize from 'resize-img'
import * as fs from 'fs'

export class EmployeeModel extends Models implements IModel {
  constructor() {
    super(REPOSITORY.EMPLOYEE.ENTITY, []);
  }

  createItem = (attrs): Promise<any> =>
    new Promise(
      async (resolve, reject): Promise<any> => {
        const {
          username,
          firstName,
          phoneNumber,
          email,
          address,
          lastName
        } = attrs;
        try {
          const employeeId = await primaryKey(
            "E",
            REPOSITORY.EMPLOYEE.PK,
            5,
            this.collection()
          );
          try {
            await this.encodePassword(attrs);
          } catch (error) {
            reject(error);
          }

          const value = {
            employeeId,
            username,
            password: attrs.password,
            email,
            _: {
              firstName,
              lastName,
              address,
              phoneNumber
            }
          };
          try {
            await this.create(value, this.collection());
            resolve();
          } catch (error) {
            reject(error);
          }
        } catch (err) {
          reject(err);
        }
      }
    );

  editItem = (id: string, attrs: Object, pk: string) =>
    new Promise(async (resolve, reject) => {
      let insertData: any = {};

      /* function check attrs for add attrs that not dubplicate */
      handlerInformationAttrs(insertData, attrs, {
        postcode: "postcode",
        prefixName: "prefixName",
        sso: "sso",
        waterBill: "waterBill",
        electricBill: "electricBill",
        startDate: "startDate",
        workStatus: "workStatus",
        overtime: "overtime",
        manDay: "manDay",
        salary: "salary",
        employeeType: "employeeType",
        position: "position",
        department: "department",
        firstName: "firstName",
        lastName: "lastName",
        address: "address",
        phoneNumber: "phoneNumber",
        tin: "tin",
        detail: "detail"
      });
      const updatedBy = attrs["updatedBy"];

      const attr = {
        ...insertData,
        _author: {
          updatedBy
        }
      };
      try {
        await editById(attr, id, pk, this.collection());
        resolve();
      } catch (error) {
        reject(error);
      }
    });

  encodePassword = (attrs: any): Promise<any> =>
    new Promise((resolve, reject) => {
      const { password } = attrs;
      bcrypt.hash(password, 12, async (err, hash) => {
        if (err) reject(`encode fail  ${err}`);
        attrs.password = hash;
        resolve("encoded");
      });
    });

  findUserByUsername = (username: string): Promise<any> =>
    new Promise(async (resolve, reject) => {
      let found;
      try {
        found = await getConnection()
          .getRepository(this.collection())
          .find({ where: { username } });
        resolve({
          status: "success",
          isFound: found.length == 0 ? false : true,
          result: found
        });
      } catch (error) {
        reject(error);
      }
    });

  generateToken = payload =>
    jwt.sign(
      { payload, role: payload.role },
      process.env.SECRET_KEY || "sellgas",
      {
        expiresIn: "5h"
      }
    );

  verifyToken = (user, passwordWithoutEncode): Promise<any> =>
    new Promise((resolve, reject) => {
      const hash = user.password;
      bcrypt.compare(passwordWithoutEncode, hash, (err, isValid) => {
        if (err) {
          return reject(`something has error \n  ${err}`);
        }

        return resolve({
          status: "Success",
          description: `Done`,
          result: isValid
        });
      });
    });

  uploadProfilePic = (id: string, uri: string) => new Promise(async (resolve, reject) => {
    try {
      const filepath = await saveImage({
        id,
        uri
      })

      resize(fs.readFileSync(filepath.toString()),{
        width: 300,
        height: 300
      }).then( buf => {
        fs.writeFile(filepath.toString(),buf,(err) => {
          if(err) console.log(err);
          console.log("already resize");
        })
      })
      const attr = {
        profilePicture: true
      }
      await editById(attr, id, REPOSITORY.EMPLOYEE.PK, this.collection())
      resolve()
    } catch (error) {
      reject(error)
    }
  })
}
