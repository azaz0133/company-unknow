import {
  IGetByEmpIdRequest,
  IUpdateEmpRequest
} from "../../interfaces/api/employee";
import { EmployeeModel } from "./model";
import { REPOSITORY } from "./../../constanst";
import { Pagination } from "./../paginate";
import { IGetRequest, IRequest } from "./../../interfaces/request";
import * as boom from "boom";
import { employeeSerializer } from "./serializer";
import * as Hapi from "hapi";
import { authorBy } from "../../utils/dbCheck/authorizeBy";
import { IResponse } from "../../interfaces/api/common";
import * as path from 'path'

const paginate = new Pagination(REPOSITORY.EMPLOYEE.ENTITY, []);

export class EmployeeController {
  getAll = async (
    req: IGetRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { page = 1, perPage = 5 } = req.query;
    let results;
    try {
      results = await paginate.paginate({}, +page, +perPage);
      results[REPOSITORY.EMPLOYEE.ENTITY].map(data => employeeSerializer(data));
      return h.response({
        status: "ok",
        statusCode: 200,
        message: "get employees data",
        results
        // j: decodeHeader(req.he)
      });
    } catch (err) {
      console.log(err);
      return boom.badRequest(err.message);
    }
    /* remove secret detail */
  };

  getById = async (
    req: IGetByEmpIdRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const empModel = new EmployeeModel();
    const { id } = req.params;
    try {
      const result = await empModel.findById(id.toUpperCase());
      employeeSerializer(result[REPOSITORY.EMPLOYEE.ENTITY]);
      return h.response({
        statusCode: 200,
        status: "ok",
        message: "success get employee by id",
        result
      });
    } catch (error) {
      return boom.notFound(error.message);
    }
  };

  update = async (
    req: IUpdateEmpRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const empModel = new EmployeeModel();
    const attrs = req.payload;
    const { id } = req.params;
    const employee = await authorBy(req.headers.authorization);
    const updatedBy = employee;
    attrs["updatedBy"] = updatedBy;
    try {
      await empModel.editItem(id, attrs, REPOSITORY.EMPLOYEE.PK);
      return h.response({
        statusCode: 202,
        status: "ok",
        message: `updated employeeId: ${id}`
      });
    } catch (error) {
      return boom.badRequest(error.message);
    }
  };

  create = async (
    req: IRequest,
    h: IResponse
  ): Promise<Hapi.ResponseObject | boom> => {
    const attrs = req.payload;
    try {
      const employeeModel = new EmployeeModel();
      await employeeModel.createItem(attrs);
      return h.response({
        statusCode: 201,
        status: "ok",
        message: "Registered Success"
      });
    } catch (err) {
      return boom.badRequest(err.message);
    }
  }

  destroy = async (
    req: IRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { id } = req.params;
    const empModel = new EmployeeModel();
    const employee = await authorBy(req.headers.authorization);
    const deleteBy = employee;

    try {
      await empModel.destroy(
        id.toUpperCase(),
        REPOSITORY.EMPLOYEE.PK,
        deleteBy
      );
      return h.response({
        statusCode: 202,
        status: "ok",
        message: `deleted employeeId : ${id}`
      });
    } catch (err) {
      return boom.badRequest(err.message);
    }
  };

  upload = async (
    req: IRequest,
    h: IResponse
  ): Promise<Hapi.ResponseObject | boom> => {
    const { id } = req.params
    const { uri }: any = req.payload
    const emp = new EmployeeModel();
    const employee = await authorBy(req.headers.authorization);
    try {
      await emp.uploadProfilePic(id, uri)
      return h.response({
        statusCode: 202,
        status: " updated",
        message: "uploaded image"
      })
    } catch (error) {
      console.log(error);
      return boom.badData(error.message)

    }
  }

  profilePicture = async (
    req: IRequest,
    h: any
  ) => {
    const { params: { id } } = req
    const emp = new EmployeeModel();
    try {
      const p = path.join(path.dirname(process.mainModule.filename),'assets','upload','profilePicture')
      return h.file(`${p}/${id}.jpg`)
    } catch (error) {
      return boom.badData(error)
    }
  }
}
