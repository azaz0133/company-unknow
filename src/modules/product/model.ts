import { Models } from "./../model";
import { IModel } from "../../interfaces/model";
import { REPOSITORY } from "../../constanst";
import { primaryKey } from "../../utils/dbCheck/generatePk";
import { editById } from "../../utils/dbCheck/editByid";

export class EProductModel extends Models implements IModel {
  constructor() {
    super(REPOSITORY.PRODUCT.ENTITY, []);
  }

  createItem = attrs =>
    new Promise(async (resolve, reject) => {
      const value = attrs;
      try {
        const productId = await primaryKey(
          "P",
          REPOSITORY.PRODUCT.PK,
          8,
          this.collection()
        );
        value.productId = productId;
        try {
         const result = await this.create(value, this.collection());
          resolve(result);
        } catch (error) {
          reject(error);
        }
      } catch (error) {
        reject(error);
      }
    });

  editItem = (id: string, attrs: Object, pk: string) =>
    new Promise(async (resolve, reject) => {
      // let insertData: any = ;
      const updatedBy = attrs["updatedBy"];
      const attr = {
        ...attrs,
        _author: {
          updatedBy
        }
      };
      try {
        await editById(attr, id, pk, this.collection());
        resolve("edited");
      } catch (error) {
        reject(error);
      }
    });
}
