import { IRequestId } from "./../../interfaces/api/common";
import { Pagination } from "./../paginate";
import { IRequest } from "./../../interfaces/request";
import { IRequestProduct } from "./../../interfaces/api/product";
import { EProductModel } from "./model";
import * as Hapi from "hapi";
import * as boom from "boom";
import { IRequestCreateProduct } from "../../interfaces/api/product";
import { controller } from "../../interfaces/controller";
import { REPOSITORY } from "../../constanst";
import { IGetParamsRequest } from "../../interfaces/api/params";
import { IProduct } from "../../interfaces/product/product";
import { authorBy } from "../../utils/dbCheck/authorizeBy";

export class ProductController implements controller {
  create = async (
    req: IRequestCreateProduct,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const attrs = req.payload;
    const employee = await authorBy(req.headers.authorization);
    attrs["createdBy"] = employee;
    if (
      attrs["changePrice"] === undefined ||
      attrs["changePrice"] == null ||
      attrs["changePrice"] < 1
    ) {
      attrs["finalPrice"] = attrs.mainPrice;
    } else {
      if (attrs["changePrice"] > attrs["mainPrice"]) {
        return boom.badData("wrong change price");
      }
      attrs["finalPrice"] = attrs.mainPrice - attrs["changePrice"];
    }
    try {
      const result = await new EProductModel().createItem(attrs);
      return h.response({
        statusCode: 201,
        status: "created",
        message: "created product",
        result
      });
    } catch (error) {
      return boom.badRequest(error);
    }
  };

  getById = async (
    req: IRequestId,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { id } = req.params;
    try {
      const result = await new EProductModel().findById(id.toUpperCase());
      return h.response({
        statusCode: 200,
        status: "ok",
        message: "success get product by id",
        result
      });
    } catch (error) {
      return boom.notFound(error.message);
    }
  };

  getAll = async (
    req: IRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { page = 1, perPage = 5 } = req.params;

    try {
      const results = await new Pagination(
        REPOSITORY.PRODUCT.ENTITY,
        []
      ).paginate({}, +page, +perPage);
      return h.response({
        statusCode: 200,
        status: "ok",
        message: "get all product",
        results
      });
    } catch (error) {
      return boom.notFound(error.message);
    }
  };

  update = async (
    req: IRequestProduct<IProduct>,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { id } = req.params;
    const productModel = new EProductModel();
    const attrs = req.payload;
    const employee = await authorBy(req.headers.authorization);
    if (
      attrs["changePrice"] === undefined ||
      attrs["changePrice"] == null ||
      attrs["changePrice"] < 1
    ) {
      attrs["finalPrice"] = attrs["mainPrice"];
    } else {
      if (attrs["changePrice"] > attrs["mainPrice"]) {
        return boom.badData("wrong change price");
      }
      attrs["finalPrice"] = attrs["mainPrice"] - attrs["changePrice"];
    }
    attrs["updatedBy"] = employee;
    try {
      await productModel.editItem(id, attrs, REPOSITORY.PRODUCT.PK);
      return h.response({
        statusCode: 202,
        status: "updated",
        message: "updated productId : " + id
      });
    } catch (error) {
      return boom.badRequest(error.message);
    }
  };

  destroy = async (
    req: IGetParamsRequest,
    h: Hapi.ResponseToolkit
  ): Promise<Hapi.ResponseObject | boom> => {
    const { id } = req.params;
    const productModel = new EProductModel();
    const employee = await authorBy(req.headers.authorization);
    const deletedBy = employee;
    try {
      await productModel.destroy(
        id.toUpperCase(),
        REPOSITORY.PRODUCT.PK,
        deletedBy
      );
      return h.response({
        statusCode: 202,
        status: "ok",
        message: `deleted productId : ${id}`
      });
    } catch (err) {
      return boom.badRequest(err.message);
    }
  };
}
