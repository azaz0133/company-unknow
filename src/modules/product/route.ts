import * as Hapi from "hapi";
import * as Joi from "joi";
import { API, HTTPVERB } from "./../../constanst";
import { productValidate } from "./validator";
import { ProductController } from "./controller";

const productController = new ProductController();
export const productRoute: Array<Hapi.ServerRoute> = [
  {
    method: HTTPVERB.POST,
    path: `${API}/product/create`,
    options: {
      description: "Create Product",
      validate: {
        payload: productValidate.create.payload
      },
      tags: ["api", "product"]
    },
    handler: productController.create
  },
  {
    method: HTTPVERB.GET,
    path: `${API}/product/{id}`,
    options: {
      description: "get product by id ",
      validate: {
        params: productValidate.getOne.params
      },
      tags: ["api", "product"]
    },
    handler: productController.getById
  },
  {
    method: HTTPVERB.GET,
    path: `${API}/product`,
    options: {
      description: "get all product",
      validate: {
        query: productValidate.getAll.query
      },
      tags: ["api", "product"]
    },
    handler: productController.getAll
  },
  {
    method: HTTPVERB.PATCH,
    path: `${API}/product/edit/{id}`,
    options: {
      description: "edit product",
      validate: {
        params: productValidate.editProduct.params,
        payload: productValidate.editProduct.payload
      },
      tags: ["api", "product"]
    },
    handler: productController.update
  },
  {
    method: HTTPVERB.DELETE,
    path: `${API}/product/delete/{id}`,
    options: {
      description: "soft delete",
      validate: {
        params: Joi.object({
          id: Joi.string()
            .required()
            .example("P0000001")
        })
      },
      tags: ["api", "product"]
    },
    handler: productController.destroy
  }
];
