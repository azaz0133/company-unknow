import * as Joi from "joi";

export const productValidate = {
  create: {
    payload: Joi.object({
      productType: Joi.string()
        .required()
        .example("gas..."),
      size: Joi.number()
        .min(0)
        .max(999)
        .required()
        .example(50),
      type: Joi.string()
        .required()
        .example("ssssss"),
      brand: Joi.string().required(),
      mainPrice: Joi.number()
        .min(0)
        .required()
        .example(20000),
      changePrice: Joi.number()
        .required()
        .min(0)
        .example(20000)
    }),
    headers: Joi.object({
      authorization: Joi.any().required(),
      "Content-Type": Joi.string().required()
    })
  },
  getOne: {
    params: Joi.object({
      id: Joi.string()
        .max(10)
        .required()
        .example("P0000001")
    })
  },

  getAll: {
    query: Joi.object({
      page: Joi.number()
        .min(0)
        .max(999)
        .optional()
        .example(1),
      perPage: Joi.number()
        .optional()
        .min(0)
        .max(999)
        .example(5)
    })
  },

  editProduct: {
    params: Joi.object({
      id: Joi.string()
        .max(10)
        .required()
        .example("P0000001")
    }),
    payload: Joi.object({
      productType: Joi.string()
        .required()
        .example("gas..."),
      size: Joi.number()
        .max(999)
        .required()
        .example(50),
      type: Joi.string()
        .required()
        .example("ssssss"),
      brand: Joi.string().required(),
      mainPrice: Joi.number()
        .min(0)
        .required()
        .example(20000),
      changePrice: Joi.number()
        .min(0)
        .required()
        .example(20000)
    }),
    headers: Joi.object({
      authorization: Joi.string().required()
    })
  },
  destroy: Joi.object({
    params: {
      id: Joi.string().required()
    },
    headers: Joi.object({
      authorization: Joi.string().required()
    })
  })
};
