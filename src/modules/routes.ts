import { employeeRoute } from "./employee/route";
import { productRoute } from "./product/route";
import { sessionRoute } from "./session/route";
import { customerRoute } from "./customer/route";
import { purchaseOrderRoute } from "./purchaseorder/route";
import { productOrderRoute } from "./productInPo/routes";
import { supplierRoutes } from "./supplier/route";
import { discountRoute } from "./discount/byCustomer/route";
import { discountProductRoute } from "./discount/byProduct/route";
import { trickRoute } from "./trick/route";

export const routes = [
  ...purchaseOrderRoute,
  ...customerRoute,
  ...employeeRoute,
  ...productRoute,
  ...sessionRoute,
  ...productOrderRoute,
  ...supplierRoutes,
  ...discountRoute,
  ...discountProductRoute,
  ...trickRoute
];
