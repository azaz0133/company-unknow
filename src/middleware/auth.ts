import { EmployeeModel } from "../modules/employee/model";
const employeeModel = new EmployeeModel();

export const authCheck = async (decoded, req) => {
  console.log("authenticating ... ");
  let authCheck;
  try {
    authCheck = await employeeModel.findUserByUsername(
      decoded["payload"]["username"]
    );
  } catch (err) {
    return { isValid: false };
  }

  return {
    isValid: authCheck["isFound"]
  };
};
