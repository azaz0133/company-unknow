import { routes } from './modules/routes'
import {initServer} from './server'

initServer(routes)