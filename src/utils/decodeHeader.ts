import * as jwt from "jsonwebtoken";

export function decodeHeader(encrypt) {
  const accessToken = encrypt.match(/Bearer (.*)/)[1];
  return jwt.decode(accessToken);
}
