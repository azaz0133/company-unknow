import * as saving from 'base64-img'
import * as path from 'path'


export const saveImage = (data: any) => new Promise((resolve, reject) => {
    const { uri, id } = data
    const p = path.join(path.dirname(process.mainModule.filename),'assets','upload','profilePicture')
    saving.img(uri,p, id, (err, filepath) => {
        if (err) reject(err)
        resolve(filepath)
    })
})