import * as PDFDocument from "pdfkit";
import * as fs from "fs";
import * as path from "path";

const doc = new PDFDocument();
const asset = path.join(
  path.dirname(require.main.filename),
  "../",
  "../",
  "assets"
);

console.log(asset);
doc.pipe(fs.createWriteStream(`${asset}/invoices/test.pdf`));

//ตัวแปรไว้ใส่ค่าหรือไว้แก้

const companyName = "บริษัทตัวอย่างเพื่อการพิมพ์เอกสารจำกัด";
const taxNumber = "123456789123";
const address =
  "บ้านเลขที่ 44/112 หมู่บ้าน วงศรียา ถนน แปดสิบหา ซอย คิดไม่ออก หมู่ที่9 ตำบน บางคุก อำเภอ เมืองฉะเชิงเทรา จังหวัดฉะเชิงเทรา 24000";
const telephoneNumber = "12345678912345";
const fax = "12345678912345";

const lorem =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in suscipit purus.  Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus nec hendrerit felis. Morbi aliquam facilisis risus eu lacinia. Sed eu leo in turpis fringilla hendrerit. Ut nec accumsan nisl.";
const customerCode = "ตัวแปรรหัสลูกค้า";
const customerName = "ชตัวแปรื่อลูกค้า";
const customerAddress = lorem;

const cash = "";
const bank = "";

//ส่วนของหัวกระดาษ
doc.image(`${asset}/logo_test.png`, 50, 40, {
  fit: [70, 50]
});

doc.font(`${asset}/angsa.ttf`);
doc.fontSize(11).text("ชื่อบริษัท  " + companyName, 130, 45);

doc.fontSize(11).text("เลขประจำผู้เสียภาษี " + taxNumber, 420, 45);

doc.fontSize(11).text("ที่อยู่", 130, 65);

doc.fontSize(11).text(address, 150, 65);

doc.fontSize(11).text("โทร " + telephoneNumber, 130, 85);

doc.fontSize(11).text("แฟกซ์ " + fax, 380, 85);

doc
  .moveTo(120, 100)
  .lineTo(540, 100)
  .stroke();

//ส่วนของข้อมูลลูกค้า
doc.fontSize(18).text("ใบแจ้งหนี้ INVOICE ", 250, 115);

doc.fontSize(12).text("รหัสลูกค้า ", 90, 160);
doc.fontSize(12).text(customerCode, 130, 160, {
  width: 250,
  align: "left",
  columns: 1,
  height: 30
});
doc.fontSize(12).text("ชื่อลูกค้า ", 90, 185);
doc.fontSize(12).text(customerName, 130, 185, {
  width: 250,
  align: "left",
  columns: 1,
  height: 30
});
doc.fontSize(12).text("ที่อยู่ลูกค้า ", 90, 210);
doc.fontSize(12).text(customerAddress, 130, 210, {
  width: 250,
  align: "left",
  columns: 1,
  height: 50
});

doc.fontSize(12).text("เลขที่ ", 400, 160);
doc.fontSize(12).text(customerCode, 440, 160, {
  width: 120,
  align: "left",
  columns: 1,
  height: 30
});
doc.fontSize(12).text("วันที่ ", 400, 185);
doc.fontSize(12).text(customerName, 440, 185, {
  width: 120,
  align: "left",
  columns: 1,
  height: 30
});
doc.fontSize(12).text("ชื่อพนักงาน ", 400, 210);
doc.fontSize(12).text(customerAddress, 440, 210, {
  width: 120,
  align: "left",
  columns: 1,
  height: 50
});

//ตารางสินค้า

doc.lineWidth(1);
doc
  .lineJoin("miter")
  .rect(70, 260, 70, 280)
  .stroke();
doc.fontSize(12).text("รหัสสินค้า ", 80, 270);
doc.fontSize(12).text("ITEM CODE ", 80, 290);

doc.rect(140, 260, 190, 280).stroke();
doc.fontSize(12).text("รายการ ", 150, 270);
doc.fontSize(12).text("DESCRIPTION", 150, 290);

doc
  .lineJoin("miter")
  .rect(330, 260, 70, 280)
  .stroke();
doc.fontSize(12).text("จำนวน ", 340, 270);
doc.fontSize(12).text("QUANTITY ", 340, 290);

doc
  .lineJoin("miter")
  .rect(400, 260, 70, 280)
  .stroke();
doc.fontSize(12).text("ราคาต่อหน่วย ", 410, 270);
doc.fontSize(12).text("UNIT PRICE ", 410, 290);

doc
  .lineJoin("miter")
  .rect(470, 260, 70, 280)
  .stroke();
doc.fontSize(12).text("จำนวนเงิน ", 480, 270);
doc.fontSize(12).text("AMOUNT ", 480, 290);

doc
  .moveTo(70, 325)
  .lineTo(540, 325)
  .stroke();

doc
  .lineJoin("miter")
  .rect(400, 540, 140, 30)
  .stroke();
doc.fontSize(12).text("รวมเงิน ", 410, 540);

doc
  .lineJoin("miter")
  .rect(400, 570, 140, 30)
  .stroke();
doc.fontSize(12).text("VAT 7% ", 410, 570);

doc
  .lineJoin("miter")
  .rect(400, 600, 140, 30)
  .stroke();
doc.fontSize(12).text("จำนวนเงินทั้งสิ้น ", 410, 600);

doc
  .lineJoin("miter")
  .rect(470, 260, 70, 370)
  .stroke();

doc
  .lineJoin("miter")
  .rect(400, 645, 140, 30)
  .stroke();
doc.fontSize(10).text("ผู้รับมอบอำนาจ ", 410, 645);
doc
  .lineJoin("miter")
  .rect(400, 675, 140, 30)
  .stroke();
doc.fontSize(10).text("ผู้รับเงิน ", 410, 675);
doc
  .lineJoin("miter")
  .rect(400, 705, 140, 30)
  .stroke();
doc.fontSize(10).text("วันรับเงิน ", 410, 705);

doc
  .lineJoin("miter")
  .rect(70, 555, 315, 180)
  .stroke();
doc.fontSize(14).text("  ชำระเงินโดย ", 70, 555);
doc.fontSize(13).text("เงินสด เป็นจำนวนเงิน ", 90, 580);
doc.fontSize(13).text(cash, 180, 580);
doc.fontSize(13).text("เช็ก", 90, 610);
doc.fontSize(13).text("ธนาคาร " + bank, 110, 610);
doc.fontSize(13).text("สาขา " + bank, 250, 610);

doc.fontSize(13).text("ลงวันที่ " + bank, 110, 640);
doc.fontSize(13).text("เลขที่ " + bank, 250, 640);

doc.fontSize(13).text("จำนวนเงิน " + bank, 110, 670);
doc.fontSize(13).text("บาท ", 250, 670);
doc.end();
