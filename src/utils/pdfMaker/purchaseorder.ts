import * as PDFDocument from "pdfkit";
import * as fs from "fs";
import * as path from "path";
import { PurchaseOrderPDF } from "../../entity/pdfMaker/purchaseOrder";
import { removeFile } from "../removefile";

export const createPurchaseOrderPDF = (attrs: PurchaseOrderPDF) =>
  new Promise((resolve, reject) => {

    removeFile("purchaseOrder")
    
    const { customer, purchaseOrder, products } = attrs;
    const customerAddress = customer.address;
    const customerCode = customer.id;
    const customerName = customer.name;
    const taxNumber = "123456789123";
    const companyName = "บริษัทตัวอย่างเพื่อการพิมพ์เอกสารจำกัด";
    const telephoneNumber = "12345678912345";
    const address =
      "sdadsaksdaklsdaklsdaklsdaklsda sadsdasda dasasd sdasdasdasdasdasda";
    const fax = "12345678912345";
    let cash = ""; //เงินสดเป็นจำนวนเงิน
    let bank = ""; //เช็ก ธนาคาร
    let branch = " "; //เช็ก เลขที่
    let date = " "; //เช็ก วันที่
    let number = ""; //เลขที่เช็ก
    let money = ""; //จำนวนเงินของเช็ก
    const product = products;
    const doc = new PDFDocument();
    const asset = path.join(path.dirname(require.main.filename), "assets");
    let pathSave = `${asset}/purchaseOrder/${purchaseOrder.id}.pdf`;
    try {
      fs.readFileSync(pathSave);
      resolve(pathSave);
    } catch (error) {
      const writeStream = fs.createWriteStream(pathSave);
      doc.pipe(writeStream);
      doc.font(`${asset}/angsa.ttf`);
      //ตัวแปรไว้เปลียนหน้า
      let count = 0;
      let sum = product.length;
      let page = 1;
      try {
        while (sum > 12) {
          //ส่วนของหัวกระดาษ
          doc.image(`${asset}/logo_test.png`, 50, 40, {
            fit: [70, 50]
          });
          doc.fontSize(11).text("ชื่อบริษัท  " + companyName, 130, 45);

          doc.fontSize(11).text("เลขประจำผู้เสียภาษี " + taxNumber, 420, 45);

          doc.fontSize(11).text("ที่อยู่", 130, 65);

          doc.fontSize(11).text(address, 150, 65);

          doc.fontSize(11).text("โทร " + telephoneNumber, 130, 85);

          doc.fontSize(11).text("แฟกซ์ " + fax, 380, 85);

          doc
            .moveTo(120, 100)
            .lineTo(540, 100)
            .stroke();

          //ส่วนของข้อมูลลูกค้า
          doc.fontSize(18).text("ใบสั่งซื้อ purchase order ", 250, 115);

          doc.fontSize(12).text("รหัสลูกค้า ", 90, 160);
          doc.fontSize(12).text(customerCode, 130, 160, {
            width: 250,
            align: "left",
            columns: 1,
            height: 30
          });
          doc.fontSize(12).text("ชื่อลูกค้า ", 90, 185);
          doc.fontSize(12).text(customerName, 130, 185, {
            width: 250,
            align: "left",
            columns: 1,
            height: 30
          });
          doc.fontSize(12).text("ที่อยู่ลูกค้า ", 90, 210);
          doc.fontSize(12).text(customerAddress, 130, 210, {
            width: 250,
            align: "left",
            columns: 1,
            height: 50
          });

          doc.fontSize(12).text("เลขที่ ", 400, 160);
          doc.fontSize(12).text(customerCode, 440, 160, {
            width: 120,
            align: "left",
            columns: 1,
            height: 30
          });
          doc.fontSize(12).text("วันที่ ", 400, 185);
          doc.fontSize(12).text(customerName, 440, 185, {
            width: 120,
            align: "left",
            columns: 1,
            height: 30
          });
          doc.fontSize(12).text("ชื่อพนักงาน ", 400, 210);
          doc.fontSize(12).text(customerAddress, 440, 210, {
            width: 120,
            align: "left",
            columns: 1,
            height: 50
          });

          doc.lineWidth(1);
          //ชื่อหัวตาราง
          doc

            .fontSize(12)
            .text("รหัสสินค้า ", 80, 270)
            .text("ITEM CODE ", 80, 280)
            .text("รายการ ", 150, 270)
            .text("DESCRIPTION", 150, 280)
            .text("จำนวน ", 340, 270)
            .text("QUANTITY ", 340, 280)
            .text("ราคาต่อหน่วย ", 410, 270)
            .text("UNIT PRICE ", 410, 280)
            .text("จำนวนเงิน ", 480, 270)
            .text("AMOUNT ", 480, 280);

          //ลากเส้นตารางสินค้า
          doc
            .moveTo(70, 260)
            .lineTo(540, 260)
            .stroke();
          doc
            .moveTo(70, 300)
            .lineTo(540, 300)
            .stroke();
          doc
            .moveTo(70, 735)
            .lineTo(540, 735)
            .stroke();
          doc
            .moveTo(70, 260)
            .lineTo(70, 735)
            .stroke();
          doc
            .moveTo(140, 260)
            .lineTo(140, 735)
            .stroke();
          doc
            .moveTo(330, 260)
            .lineTo(330, 735)
            .stroke();
          doc
            .moveTo(400, 260)
            .lineTo(400, 735)
            .stroke();
          doc
            .moveTo(470, 260)
            .lineTo(470, 735)
            .stroke();
          doc
            .moveTo(540, 260)
            .lineTo(540, 735)
            .stroke();

          //รายการสินค้าแบบไม่เกิน20ชิ้น
          for (var i = 0; !(sum < 1 || i > 20); i++, count++, sum--) {
            doc
              .fontSize(12)
              .text(product[count].productId, 80, 300 + 20 * i)
              .text(product[count].productType, 150, 300 + 20 * i)
              .text(product[count].quantity, 340, 300 + 20 * i)
              .text(product[count].finalPrice, 410, 300 + 20 * i)
              .text(product[count].amount, 480, 300 + 20 * i);
          }

          doc.fontSize(11).text("หน้าที่ " + page, 500, 20);
          page++;
          doc.addPage();
        }
        //หน้าสุดท้าย
        //ส่วนของหัวกระดาษ
        doc.image(`${asset}/logo_test.png`, 50, 40, {
          fit: [70, 50]
        });
        doc.fontSize(11).text("ชื่อบริษัท  " + companyName, 130, 45);

        doc.fontSize(11).text("เลขประจำผู้เสียภาษี " + taxNumber, 420, 45);

        doc.fontSize(11).text("ที่อยู่", 130, 65);

        doc.fontSize(11).text(address, 150, 65);

        doc.fontSize(11).text("โทร " + telephoneNumber, 130, 85);

        doc.fontSize(11).text("แฟกซ์ " + fax, 380, 85);

        doc
          .moveTo(120, 100)
          .lineTo(540, 100)
          .stroke();

        //ส่วนของข้อมูลลูกค้า
        doc.fontSize(18).text("ใบสั่งซื้อ purchase order ", 250, 115);

        doc.fontSize(12).text("รหัสลูกค้า ", 90, 160);
        doc.fontSize(12).text(customerCode, 130, 160, {
          width: 250,
          align: "left",
          columns: 1,
          height: 30
        });
        doc.fontSize(12).text("ชื่อลูกค้า ", 90, 185);
        doc.fontSize(12).text(customerName, 130, 185, {
          width: 250,
          align: "left",
          columns: 1,
          height: 30
        });
        doc.fontSize(12).text("ที่อยู่ลูกค้า ", 90, 210);
        doc.fontSize(12).text(customerAddress, 130, 210, {
          width: 250,
          align: "left",
          columns: 1,
          height: 50
        });

        doc.fontSize(12).text("เลขที่ ", 400, 160);
        doc.fontSize(12).text(customerCode, 440, 160, {
          width: 120,
          align: "left",
          columns: 1,
          height: 30
        });
        doc.fontSize(12).text("วันที่ ", 400, 185);
        doc.fontSize(12).text(customerName, 440, 185, {
          width: 120,
          align: "left",
          columns: 1,
          height: 30
        });
        doc.fontSize(12).text("ชื่อพนักงาน ", 400, 210);
        doc.fontSize(12).text(customerAddress, 440, 210, {
          width: 120,
          align: "left",
          columns: 1,
          height: 50
        });

        doc.lineWidth(1);
        //ชื่อหัวตาราง
        doc

          .fontSize(12)
          .text("รหัสสินค้า ", 80, 270)
          .text("ITEM CODE ", 80, 280)
          .text("รายการ ", 150, 270)
          .text("DESCRIPTION", 150, 280)
          .text("จำนวน ", 340, 270)
          .text("QUANTITY ", 340, 280)
          .text("ราคาต่อหน่วย ", 410, 270)
          .text("UNIT PRICE ", 410, 280)
          .text("จำนวนเงิน ", 480, 270)
          .text("AMOUNT ", 480, 280);

        //ลากเส้นตารางสินค้า
        doc
          .moveTo(70, 260)
          .lineTo(540, 260)
          .stroke();
        doc
          .moveTo(70, 300)
          .lineTo(540, 300)
          .stroke();
        doc
          .moveTo(70, 540)
          .lineTo(540, 540)
          .stroke();

        doc
          .moveTo(70, 260)
          .lineTo(70, 540)
          .stroke();
        doc
          .moveTo(140, 260)
          .lineTo(140, 540)
          .stroke();
        doc
          .moveTo(330, 260)
          .lineTo(330, 540)
          .stroke();
        doc
          .moveTo(400, 260)
          .lineTo(400, 540)
          .stroke();
        doc
          .moveTo(470, 260)
          .lineTo(470, 540)
          .stroke();
        doc
          .moveTo(540, 260)
          .lineTo(540, 540)
          .stroke();
        //รายการสินค้าแบบไม่เกิน12ชิ้น
        for (var i = 0; !(sum < 1 || i > 20); i++, count++, sum--) {
          doc

            .fontSize(12)
            .text(product[count].productId, 80, 300 + 20 * i)
            .text(product[count].productType, 150, 300 + 20 * i)
            .text(product[count].quantity, 340, 300 + 20 * i)
            .text(product[count].finalPrice, 410, 300 + 20 * i)
            .text(product[count].amount, 480, 300 + 20 * i);
        }
        doc.fontSize(11).text("หน้าที่ " + page, 500, 20);
        page++;

        //ท้ายตาราง
        //สรุปเงิน
        doc
          .lineJoin("miter")
          .rect(400, 540, 140, 30)
          .stroke();

        doc
          .fontSize(12)
          .text("รวมเงิน ", 410, 540)
          .text(purchaseOrder.withoutVat.toString(), 480, 540);

        doc
          .lineJoin("miter")
          .rect(400, 570, 140, 30)
          .stroke();
        doc
          .fontSize(12)
          .text("VAT 7% ", 410, 570)
          .text(purchaseOrder.vat.toString(), 480, 570);

        doc
          .lineJoin("miter")
          .rect(400, 600, 140, 30)
          .stroke();
        doc
          .fontSize(12)
          .text("จำนวนเงินทั้งสิ้น ", 410, 600)
          .text(purchaseOrder.amount.toString(), 480, 600);

        doc
          .lineJoin("miter")
          .rect(470, 260, 70, 370)
          .stroke();

        //ช่องเช็นลายเช็น
        doc
          .lineJoin("miter")
          .rect(400, 645, 140, 30)
          .stroke();
        doc.fontSize(10).text("ผู้รับมอบอำนาจ ", 410, 645);
        doc
          .lineJoin("miter")
          .rect(400, 675, 140, 30)
          .stroke();
        doc.fontSize(10).text("ผู้รับเงิน ", 410, 675);
        doc
          .lineJoin("miter")
          .rect(400, 705, 140, 30)
          .stroke();
        doc.fontSize(10).text("วันรับเงิน ", 410, 705);

        //รายล่ะเอียดการจ่าย
        doc
          .lineJoin("miter")
          .rect(70, 555, 315, 180)
          .stroke();
        doc.fontSize(14).text("  ชำระเงินโดย ", 70, 555);
        doc.fontSize(13).text("เงินสด เป็นจำนวนเงิน ", 90, 580);
        doc.fontSize(13).text(cash, 180, 580);
        doc.fontSize(13).text("เช็ก", 90, 610);
        doc.fontSize(13).text("ธนาคาร " + bank, 110, 610);
        doc.fontSize(13).text("สาขา " + branch, 250, 610);

        doc.fontSize(13).text("ลงวันที่ " + date, 110, 640);
        doc.fontSize(13).text("เลขที่ " + number, 250, 640);

        doc.fontSize(13).text("จำนวนเงิน " + money, 110, 670);
        doc.fontSize(13).text("บาท ", 250, 670);

        doc.end();

        writeStream.on("finish", (data) => {
          console.log(data);
          resolve(pathSave);
        });
      } catch (error) {
        reject(error);
      }
    }
  });
