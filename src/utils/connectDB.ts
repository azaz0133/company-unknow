import { createConnection } from "typeorm";

export const connectionToDatabase = (): Promise<any> =>
  createConnection({
    type: "mysql",
    host: process.env.DB_HOST || "myadmin.lattesoft.co.th",
    port: parseInt(process.env.DB_PORT) || 3306,
    username: process.env.DB_USER || "sellgas",
    password: process.env.DB_PASS || "sellgas",
    database: process.env.DB_NAME || "sellgas_test",
    synchronize: true,
    logging: false,
    entities: ["src/entity/**/*.ts"],
    migrations: ["src/migration/**/*.ts"],
    subscribers: ["src/subscriber/**/*.ts"],
    cli: {
      entitiesDir: "src/entity",
      migrationsDir: "src/migration",
      subscribersDir: "src/subscriber"
    }
  });
