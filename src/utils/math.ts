export function padNum(num, length) {
  return Array(length + 1 - num.toString().length).join("0") + num;
}
