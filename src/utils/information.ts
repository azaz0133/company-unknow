function isInformation(d) {
  switch (d) {
    case "firstName":
    case "lastName":
    case "address":
    case "phoneNumber":
    case "tin":
    case "detail":
      return true;
    default:
      return false;
  }
}
export function handlerInformationAttrs(insertData: any, attrs: Object,abstact: Object) {
  const abstractAttrs = abstact ;
  let insertKey = Object.keys(abstractAttrs).filter(
    key => abstractAttrs[key] in attrs
  );
  for (let a in insertKey) {
    if (isInformation(insertKey[a])) {
      if (insertData._ === undefined) {
        insertData._ = {};
      }
      insertData._[insertKey[a]] = attrs[insertKey[a]];
    } else {
      insertData[insertKey[a]] = attrs[insertKey[a]];
    }
  }
}
