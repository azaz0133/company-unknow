import { checkLastPk } from "./checkLastPk";
import { padNum } from "../math";

const primaryKey = (
  prefix: string,
  pk: string,
  stop,
  collection
): Promise<string> =>
  new Promise(async (resolve, reject) => {
    let id = "";
    try {
      const lastPk = await checkLastPk(pk, collection);
      if (lastPk.length == 0) id = `${prefix}${padNum(1, stop - 1)}`;
      else {
        id = `${prefix}${padNum(
          parseInt(
            lastPk[0][pk].substring(prefix.length, stop + prefix.length - 1)
          ) + 1,
          stop - 1
        )}`;
      }
      resolve(id);
    } catch (error) {
      reject({
        status: "error",
        message: error.message
      });
    }
  });

export { primaryKey };
