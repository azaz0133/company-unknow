import { getConnection } from "typeorm";
import { decodeHeader } from "../decodeHeader";
import { REPOSITORY } from "../../constanst";

export const authorBy = async key => {
  try {
    const empId = await decodeHeader(key);
    const id = empId["payload"][REPOSITORY.EMPLOYEE.PK];
    try {
      const employee = await getConnection()
        .getRepository(REPOSITORY.EMPLOYEE.ENTITY)
        .findOne(id, {
          where: {
            _c: {
              useFlag: true
            }
          }
        });
      delete employee["password"];
      return employee;
    } catch (error) {
      return error.message;
    }
  } catch (error) {}
};
