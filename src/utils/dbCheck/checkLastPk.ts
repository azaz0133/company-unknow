import { getConnection } from "typeorm";

export const checkLastPk = (pkName: string, collection): Promise<Array<any>> =>
  new Promise(async resolve => {
    const find = await getConnection()
      .getRepository(collection)
      .find({
        order: {
          [pkName]: "DESC"
        }
      });
    resolve(find);
  });
