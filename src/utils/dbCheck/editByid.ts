import { getConnection } from "typeorm";

const editById = (
  insertData: Object,
  id: string,
  pk: string,
  collection: any
): Promise<string> =>
  new Promise(async (resolve, reject) => {
    try {
      const result = (await getConnection()
        .createQueryBuilder()
        .update(collection)
        .set(insertData)
        .where(`${pk} = :id`, {
          id
        })
        .andWhere(`_c.useFlag = true`)
        .execute()).raw;

      if (result.changedRows == 0) reject("not found");
      resolve(result);
    } catch (error) {
      reject(error);
    }
  });

export { editById };
