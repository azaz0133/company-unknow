const descripApi = [
  {
    name: "session",
    description: "about session"
  }
];

const swaggerOptions = {
  info: {
    title: "Api for Sell Gas System",
    version: "1.0",
    description:
      "this is api reference use for know how to use api for sell gas company only",
    contact: {
      name: "AliasName : azaz0133",
      email: "anirut.workspace@gmail.com"
    }
  },
  tags: descripApi,
  grouping: "tags"
};

export { swaggerOptions };
