import { Checker } from "../common/checker";
import {
  Entity,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  JoinTable
} from "typeorm";
import { IProductInPO } from "../../interfaces/product/productInPO";
import { EPurchaseOrder } from "../purchaseOrder";
import { EProduct } from "./product";

@Entity()
export class EProductInPO implements IProductInPO {
  @PrimaryGeneratedColumn()
  id: string;

  @Column(type => Checker)
  _c: Checker;

  @Column({
    type: "int",
    nullable: false
  })
  total: number;

  @ManyToOne(type => EPurchaseOrder, EPO => EPO.purchaseOrderInPO)
  fk1: EPurchaseOrder;

  @ManyToOne(type => EProduct, EP => EP.productInPO)
  fk2: EProduct;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
