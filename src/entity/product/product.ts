import { Checker } from "../common/checker";
import { Column, PrimaryColumn, Entity, OneToMany } from "typeorm";
import { IProduct } from "../../interfaces/product/product";
import { EProductInPO } from "./productInPO";
import { Author } from "../common/authorization";
import { ECustomerProductDiscount } from "../customer/ECustomerProductDiscount";

@Entity()
export class EProduct implements IProduct {
  @PrimaryColumn({
    length: 25
  })
  productId: string;

  @Column({
    length: 25,
    nullable: false
  })
  productType: string;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: true
  })
  size: number;

  @Column({
    length: 50,
    nullable: true
  })
  type: string;

  @Column({
    length: 50,
    nullable: true
  })
  brand: string;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: true
  })
  mainPrice: number;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: true
  })
  changePrice: number;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: true
  })
  finalPrice: number;

  @Column(type => Checker)
  _c: Checker;

  @OneToMany(type => ECustomerProductDiscount, ECPD => ECPD.product)
  discountFromProduct: ECustomerProductDiscount[];

  @OneToMany(type => EProductInPO, EIPO => EIPO.fk2)
  productInPO: EProductInPO[];

  @Column(type => Author)
  _author: Author;
}
