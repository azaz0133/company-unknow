import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { Author } from "../common/authorization";
import { Checker } from "../common/checker";
import { ECustomer } from "./customer";

@Entity()
export class ECustomerInProductOrder {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: false
  })
  changeByPtt: number;

  @Column({
    type: "decimal",
    nullable: false,
    precision: 10,
    scale: 2
  })
  changeByCustomer: number;

  @Column({
    type: "decimal",
    nullable: false,
    precision: 10,
    scale: 2
  })
  finallyPrice: number;

  @Column(type => Author)
  _author: Author;

  @Column(type => Checker)
  _c: Checker;
}
