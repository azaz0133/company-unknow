import { EPurchaseOrder } from "../purchaseOrder";
import { Checker } from "../common/checker";
import { Infomation } from "../common/infomationUser";
import { Entity, Column, OneToMany, PrimaryColumn, ManyToOne } from "typeorm";
import { ICustomer } from "../../interfaces/user/customer";
import { Author } from "../common/authorization";
import { ECustomerDiscount } from "./customerDiscount";
import { ECustomerProductDiscount } from "./ECustomerProductDiscount";
@Entity()
export class ECustomer implements ICustomer {
  @PrimaryColumn({
    length: 5
  })
  customerId: string;

  @Column({
    length: 50,
    nullable: true
  })
  prefixName: string;

  @Column({
    type: "text",
    nullable: true
  })
  organization: string;

  @Column({
    nullable: true
  })
  initialsName: string;

  @Column(type => Infomation)
  _: Infomation;

  @Column({
    length: 50,
    nullable: true
  })
  branch: string;

  @Column({
    length: 15,
    nullable: true
  })
  faxNumber: string;

  @Column({
    length: 50,
    nullable: true
  })
  contact: string;

  @Column({
    nullable: true
  })
  latitude: number;

  @Column({
    nullable: true
  })
  longtitude: number;

  @Column({
    type: "text",
    nullable: true
  })
  cashCredit: string;

  @Column({
    length: 50,
    nullable: true
  })
  email: string;

  @Column({
    nullable: true
  })
  amountCredit: number;

  @Column({
    type: "text",
    nullable: true
  })
  paymentConditions: string;

  @Column({
    type: "text",
    nullable: true
  })
  gasCalculation: string;

  @Column({
    type: "text",
    nullable: true
  })
  priceDetermination: string;

  @Column({
    type: "text",
    nullable: true
  })
  supportBrand: string;

  @Column({
    length: 20,
    nullable: true
  })
  vatType: string;

  @Column({
    nullable: true
  })
  decimalType: number;

  @Column({
    nullable: true
  })
  getProductType: string;

  @Column({
    length: 20,
    nullable: true
  })
  customerType: string;

  @Column({
    nullable: true
  })
  tranferFee: number;

  @Column(type => Checker)
  _c: Checker;

  @OneToMany(type => ECustomerDiscount, ECdis => ECdis.customerDiscount)
  discount: ECustomerDiscount[];

  @OneToMany(type => ECustomerProductDiscount, ECPd => ECPd.customers)
  productDiscount: ECustomerProductDiscount[];

  @OneToMany(type => EPurchaseOrder, Epo => Epo.fk1)
  CustomerPo: EPurchaseOrder[];

  @Column(type => Author)
  _author: Author;
}
