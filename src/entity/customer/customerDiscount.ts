import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { Author } from "../common/authorization";
import { Checker } from "../common/checker";
import { ECustomer } from "./customer";

@Entity()
export class ECustomerDiscount {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: 20,
    nullable: true
  })
  year: string;

  @Column({
    length: 20,
    nullable: true
  })
  month: string;

  @Column({
    type:"date",
    nullable: true
  })
  date: Date;

  @Column({
    type: "decimal",
    nullable: false,
    precision: 10,
    scale: 2
  })
  discount: number;

  @ManyToOne(type => ECustomer, EC => EC.discount)
  customerDiscount: ECustomer;

  @Column(type => Author)
  _author: Author;

  @Column(type => Checker)
  _c: Checker;
}
