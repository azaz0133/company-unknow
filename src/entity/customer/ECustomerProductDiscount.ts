import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { Author } from "../common/authorization";
import { Checker } from "../common/checker";
import { ECustomer } from "./customer";
import { EProduct } from "../product/product";

@Entity()
export class ECustomerProductDiscount {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: false
  })
  changeByCustomer: number;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: false
  })
  finalPrice: number;

  @ManyToOne(type => EProduct, EP => EP.discountFromProduct)
  product: EProduct;

  @ManyToOne(type => ECustomer, EC => EC.productDiscount)
  customers: ECustomer;

  @Column(type => Checker)
  _c: Checker;

  @Column(type => Author)
  _author: Author;
}
