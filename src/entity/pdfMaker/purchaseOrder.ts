export interface productsPdf {
  productId: string;
  productType: string;
  quantity: any;
  finalPrice: any;
  amount: any;
}

export interface PurchaseOrderPDF {
  customer: {
    name: string;
    id: string;
    address: string;
  };
  purchaseOrder?: {
    id: string;
    cash?: number;
    vat: number;
    withoutVat: number;
    amount: any;
  };
  products: Array<productsPdf>;
}
