import { EEmployee } from "./employee";
import { ECustomer } from "./customer/customer";
import { Checker } from "./common/checker";
import { Entity, Column, ManyToOne, OneToMany, PrimaryColumn } from "typeorm";
import { IPO } from "./../interfaces/product/purchaseOrder";
import { EProductInPO } from "./product/productInPO";
import { Author } from "./common/authorization";

@Entity()
export class EPurchaseOrder implements IPO {
  @PrimaryColumn({
    length: 20
  })
  purchaseOrderId: string;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: true
  })
  subTotal: number;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: true
  })
  vat: number;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: true
  })
  grandTotal: number;

  @Column({
    length: 20,
    nullable: false,
    default:"test"
  })
  billType: string;

  @Column({
    length: 20,
    nullable: false,
    default:"test"
  })
  paymentType: string;

  @Column({
    nullable: true,
    type: "decimal",
    precision: 10,
    scale: 2
  })
  arrears: string;

  @Column({
    nullable: true,
    length: 10
  })
  paymentStatus: string;

  @Column({
    nullable: true,
    length: 10
  })
  transportStatus: string;

  @Column(type => Checker)
  _c: Checker;

  @ManyToOne(type => ECustomer, EC => EC.CustomerPo)
  fk1: ECustomer;

  @ManyToOne(type => EEmployee, EE => EE.employeeId)
  fk2: EEmployee;

  @OneToMany(type => EProductInPO, EPIO => EPIO.fk1)
  purchaseOrderInPO: EProductInPO[];

  @Column(type => Author)
  _author: Author;
}
