import { Column } from "typeorm";

export class Infomation {
  @Column({
    length: 50,
    nullable: false
  })
  firstName: string;

  @Column({
    length: 50,
    nullable: false
  })
  lastName: string;

  @Column({
    type: "text",
    nullable: true
  })
  address: string;

  @Column({
    length: 10,
    nullable: true
  })
  postcode: string;

  @Column({
    length: 15,
    nullable: true
  })
  phoneNumber: string;

  @Column({
    length: 13,
    nullable: true
  })
  tin: string;

  @Column({
    type: "longtext",
    nullable: true
  })
  detail: string;
}
