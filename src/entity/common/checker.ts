import { Column, CreateDateColumn, UpdateDateColumn } from "typeorm";

export class Checker {
  @Column({
    nullable: false,
    length: 5,
    default: true
  })
  useFlag: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @Column({
    type: "datetime",
    nullable: true
  })
  deletedAt: Date;
}
