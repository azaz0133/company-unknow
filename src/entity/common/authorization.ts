import { ManyToOne } from "typeorm";
import { EEmployee } from "../employee";

export class Author {
  @ManyToOne(type => EEmployee, Eemployee => Eemployee.employeeId)
  createdBy: EEmployee;

  @ManyToOne(type => EEmployee, Eemployee => Eemployee.employeeId)
  updatedBy: EEmployee;

  @ManyToOne(type => EEmployee, Eemployee => Eemployee.employeeId)
  deletedBy: EEmployee;
}
