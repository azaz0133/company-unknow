import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { Author } from "../common/authorization";
import { Checker } from "../common/checker";

@Entity()
export class ESupplierRegularOrder {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: false
  })
  changeByPtt: number;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: false
  })
  changeByCustomer: number;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: false
  })
  finalPrice: number;

  @Column(type => Author)
  _author: Author;

  @Column(type => Checker)
  _c: Checker;
}
