import { Entity, PrimaryColumn, Column } from "typeorm";
import { Infomation } from "../common/infomationUser";
import { Author } from "../common/authorization";
import { Checker } from "../common/checker";
import { ISupplier } from "../../interfaces/supplier/supplier";

@Entity()
export class ESupplier implements ISupplier {
  @PrimaryColumn({
    length: 5
  })
  supplierId: string;

  @Column(type => Infomation)
  _: Infomation;

  @Column({
    length: 50,
    nullable: true
  })
  prefixName: string;

  @Column({
    length: 50,
    nullable: true
  })
  branch: string;

  @Column({
    length: 50,
    nullable: true
  })
  email: string;

  @Column({
    length: 50,
    nullable: true
  })
  contact: string;

  @Column({
    nullable: true
  })
  longtitde: number;

  @Column({
    nullable: true
  })
  latitude: number;

  @Column({
    type: "int",
    nullable: true
  })
  amountCredit: number;

  @Column({
    type: "text",
    nullable: true
  })
  conditionPayment: string;

  @Column({
    type: "text",
    nullable: true
  })
  calculatingGas: string;

  @Column({
    type: "text",
    nullable: true
  })
  determinationPrice: string;

  @Column({
    type: "text",
    nullable: true
  })
  supportBrand: string;

  @Column({
    length: 50,
    nullable: true
  })
  vatType: string;

  @Column({
    type: "int",
    nullable: true
  })
  decimalType: number;

  @Column({
    nullable: true
  })
  getProductType: string;

  @Column({
    length: 20,
    nullable: true
  })
  customerType: string;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 10,
    nullable: true
  })
  tranferFee: number;

  @Column({
    nullable: true
  })
  faxNumber: string;

  @Column({
    nullable: true
  })
  cashCredit: number

  @Column(type => Author)
  _author: Author;

  @Column(type => Checker)
  _c: Checker;
}
