import { EPurchaseOrder } from "./purchaseOrder";
import { Checker } from "./common/checker";
import { Infomation } from "./common/infomationUser";
import { Column, Entity, OneToMany, PrimaryColumn } from "typeorm";
import { IEmployee } from "../interfaces/user/employee";
import { Author } from "./common/authorization";

@Entity()
export class EEmployee implements IEmployee {
  @PrimaryColumn({
    length: 5
  })
  employeeId: string;

  @Column({
    length: 10,
    nullable: true,
    default: ""
  })
  prefixName: string;

  @Column({
    length: 10,
    nullable: false,
    default: "0000000"
  })
  roles: string;

  @Column({
    length: 100,
    nullable: true
  })
  email: string;

  @Column({
    length: 50,
    nullable: false,
    unique: true
  })
  username: string;

  @Column({
    length: 100,
    nullable: false
  })
  password: string;

  @Column({
    length: 5,
    nullable: true,
    default: false
  })
  profilePicture: string;

  @Column({
    length: 15,
    nullable: true
  })
  department: string;

  @Column({
    length: 15,
    nullable: true
  })
  position: string;

  @Column(type => Infomation)
  _: Infomation;

  @Column({
    length: 10,
    nullable: true
  })
  employeeType: string;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: true
  })
  salary: number;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: true
  })
  manDay: number;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: true
  })
  overtime: number;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: true
  })
  sso: number;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: true
  })
  electricBill: number;

  @Column({
    type: "decimal",
    precision: 10,
    scale: 2,
    nullable: true
  })
  waterBill: number;

  @Column({
    type: "datetime",
    nullable: true
  })
  startDate: Date;

  @Column({
    type: "datetime",
    nullable: true
  })
  ResignDate: Date;

  @Column({
    length: 5,
    nullable: false,
    default: true
  })
  workStatus: string;

  @Column(type => Checker)
  _c: Checker;

  @OneToMany(type => EPurchaseOrder, EPO => EPO.fk2)
  employeeDidPO: EPurchaseOrder[];

  @OneToMany(type => EEmployee, employee => employee.employeeId)
  whoDeleted: EEmployee;

  @Column(type => Author)
  _author: Author;
  /* Relations */

  // @OneToMany()
}
