import * as Hapi from "hapi";

export interface IRequest extends Hapi.Request {}

export interface IGetRequest extends IRequest {
  query: {
    page: string;
    perPage: string;
    customerId?: string;
  };
}
