import { Author } from "../../entity/common/authorization";
import { EProductInPO } from "../../entity/product/productInPO";
import { ECustomer } from "../../entity/customer/customer";
import { EEmployee } from "../../entity/employee";

export interface IPO {
  purchaseOrderId?: string;
  subTotal?: number;
  vat?: number;
  grandTotal?: number;
  _author?: Author;
  purchaseOrderInPO?: EProductInPO[];
  fk1?: ECustomer;
  fk2?: EEmployee;
}
