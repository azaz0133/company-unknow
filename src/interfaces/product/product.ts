import { Author } from "../../entity/common/authorization";
import { Checker } from "../../entity/common/checker";
import { EProductInPO } from "../../entity/product/productInPO";

export interface IProduct {
  productId?: string;
  productType?: string;
  size?: number;
  type?: string;
  brand?: string;
  mainPrice?: number;
  chargePrice?: number;
  finalPrice?: number;
  _author?: Author;
  _c?: Checker;
  productInPO?: EProductInPO[];
}
