import { Checker } from "../../entity/common/checker";
import { EPurchaseOrder } from "../../entity/purchaseOrder";
import { EProduct } from "../../entity/product/product";

export interface IProductInPO {
  id?: string;
  _c?: Checker;
  fk1?: EPurchaseOrder;
  fk2?: EProduct;
}
