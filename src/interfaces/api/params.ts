import { IRequest } from "../request";

export interface IGetParamsRequest extends IRequest {
  params: {
    id?: string;
  };
}
