import { IRequest } from "../request";

export interface IRequestEmpRegister extends IRequest {
  payload: {
    username: string;
    password: string;
    firstName: string;
    lastName: string;
  };
}

export interface IGetByEmpIdRequest extends IRequest {
  params: {
    id: string;
  };
}

export interface IUpdateEmpRequest extends IRequest {
  params: {
    id: string;
  };
  payload: {
    profilePicture?: string;
    firstName?: string;
    lastName?: string;
    address?: string;
    phoneNumber?: string;
    tin?: string;
    detail?: string;
  };
}
