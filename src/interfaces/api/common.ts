import { IRequest } from "./../request";
import * as Hapi from "hapi";
export interface IRequestId extends IRequest {
  params: {
    id: string;
  };
}

export interface IRequestCreate<T> extends IRequest {
  body: T;
}

export interface IRequestUpdate<T> extends IRequest {
  body: T;
  params: {
    id: string;
  };
}

export interface IResponse extends Hapi.ResponseToolkit {}
