import { IRequest } from "./../request";

export interface ISessionRequest extends IRequest {
  payload: {
    username: string;
    password: string;
  };
}
