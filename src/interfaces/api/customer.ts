import { IRequest } from "./../request";
import { IGetParamsRequest } from "./params";

export interface IRequestCreateCustomer extends IRequest {
  payload: {
    fisrtName: string;
    lastName: string;
  };
}

export interface IGetByCusIdRequest extends IGetParamsRequest {
  params: {
    id: string;
  };
  payload: {
    fisrtName?: string;
    lastName?: string;
    prefixName?: string;
    branch?: string;
    faxNumber?: string;
    contact?: string;
    lattitude?: number;
    longtitude?: number;
    cashCredit?: string;
    amountCredit?: number;
    paymentConditions?: string;
    gasCalculation?: string;
    priceDetermination?: string;
    supportBrand?: string;
    vatType?: string;
    decimalType?: number;
    getProductVatType?: number;
    customerType?: string;
    tranferFee?: number;
  };
}
