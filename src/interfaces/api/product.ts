import { IRequest } from "../request";

export interface IRequestCreateProduct extends IRequest {
  payload: {
    productType: string;
    size: number;
    type: string;
    brand: string;
    mainPrice: number;
  };
}

export interface IRequestProduct<T> extends IRequest {
  params: {
    id: string;
  };
  body: T;
}
