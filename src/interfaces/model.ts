export interface IModel {
  createItem: (attrs) => Promise<any>;

  editItem: (id: string, attrs: Object, pk: string) => Promise<any>;

  // softDelete: (req: IRequestId, h: IResponse) => Promise<Hapi.ResponseObject>;
}
