export interface controller {
  getAll: (req: any, h: any) => any;
  getById: (req: any, h: any) => any;
  create?: (req: any, h: any) => any;
  delete?: (req: any, h: any) => any;
}
