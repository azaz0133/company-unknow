import { Infomation } from "../../entity/common/infomationUser";
import { Author } from "../../entity/common/authorization";
import { Checker } from "../../entity/common/checker";

export interface ISupplier {
  supplierId?: string;
  _?: Infomation;
  prefixName?: string;
  email?: string;
  contact?: string;
  longtitde?: number;
  latitude?: number;
  amountCredit?: number;
  conditionPaymeny?: string;
  calculatingGas?: string;
  determinationPrice?: string;
  supportBrand?: string;
  vatType?: string;
  decimalType?: number;
  getProductType?: string;
  customerType?: string;
  tranferFee?: number;
  _author?: Author;
  _c?: Checker;
}
