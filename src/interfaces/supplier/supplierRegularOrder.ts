import { Author } from "../../entity/common/authorization";
import { Checker } from "../../entity/common/checker";

export interface ISupplierRegularOrder {
  id?: string;
  changeByPtt?: number;
  changeByCustomer?: number;
  finalPrice?: number;
  _author?: Author;
  _c?: Checker;
}
