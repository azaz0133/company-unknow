import { Checker } from "./../../entity/common/checker";
import { Infomation } from "./../../entity/common/infomationUser";
export interface IEmployee {
  employeeId?: string;
  roles?: string;
  username?: string;
  password?: string;
  profilePicture?: string;
  department?: string;
  position?: string;
  employeeType?: string;
  salary?: number;
  manDay?: number;
  overtime?: number;
  sso?: number;
  electricBill?: number;
  waterBill?: number;
  startDate?: Date;
  ResignDate?: Date;
  workStatus?: string;
  _c?: Checker;
  _?: Infomation;
}
