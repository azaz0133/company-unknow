import { Checker } from "./../../entity/common/checker";
import { Infomation } from "../../entity/common/infomationUser";

export interface ICustomer {
  // id: number;
  customerId: string;
  prefixName: string;
  // name: string;
  branch: string;
  // address: string;
  // tin: string;
  // postcode: string;
  // phoneNumber: string;
  faxNumber: string;
  // email: string;
  contact: string;
  latitude: number;
  longtitude: number;
  cashCredit: string;
  amountCredit: number;
  paymentConditions: string;
  gasCalculation: string;
  priceDetermination: string;
  supportBrand: string;
  vatType: string;
  decimalType: number;
  getProductType: string;
  customerType: string;
  tranferFee: number;
  _c: Checker;
  // _: Infomation;
  // detail: string;
  // useFlag: boolean;
  // createdAt: Date;
  // updatedAt: Date;
  // deletedAt: Date;
}
