import { IRequest } from "./request";

export interface Crud {
  // create>(attrs): Promise<string>;
  // update<T>(id: string | number, attrs: T): Promise<any>;
  destroy(id: string | number, pk: string, deleteBy: object): Promise<any>;
}
