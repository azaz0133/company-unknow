# Node API Starter

* This repo uses Hapi 17 and Typeorm for a starter backend.

## Environment Variables

* Instead of accessing the .env directly from ```process.env```, instead do ```import env from './configurations'```.
* This uses envalid to check the environment variables.
* To add additional env attributes, be sure to add them to ./configurations/validator.ts

## Creating New Models

* When creating new models (a.k.a. entities), be sure to extend DefaultEntity from ./utils to add a uuid Id column, createdAt, and updatedAt.

## Crud Routes

* An auto-generated crud api will form for every file in the crud folder. Follow the object shape of IGeneratedApiResource.

## Custom Routes

* All other routes and controllers should be defined in those folders respectively as normal.
